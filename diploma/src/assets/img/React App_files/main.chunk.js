(this["webpackJsonpdiploma"] = this["webpackJsonpdiploma"] || []).push([["main"],{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css":
/*!************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/App.css ***!
  \************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/Chat/Chat.module.css":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/Meetup/GuestBoard/Chat/Chat.module.css ***!
  \******************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.Chat_chatContainer__1Qyks {\n    border-top: 1px solid #707070;\n    width: 100%;\n    height: 10px;\n    flex-grow: 3;\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    padding-top: 15px;\n    margin-top: 15px;\n}\n\n.Chat_scrollBox__27Cqx {\n    overflow-y: scroll;\n    width: 100%;\n    flex-grow: 1;\n    padding-bottom: 20px;\n}\n\n.Chat_messageInput__296C1 {\n    flex-grow: 1;\n}\n\n.Chat_scrollBox__27Cqx::-webkit-scrollbar { width: 0; }\n\n.Chat_scrollBox__27Cqx { -ms-overflow-style: none; }\n\n.Chat_scrollBox__27Cqx { overflow: -moz-scrollbars-none; }\n\n.Chat_messageBox__TSLET {\n    display: flex;\n    align-items: flex-start;\n    justify-content: space-between;\n    margin-bottom: 20px;\n}\n\n.Chat_messageBox__TSLET p {\n    font-size: 14px;\n    margin-top: 10px;\n    white-space: pre-wrap;\n    font-family: Arial, Helvetica, sans-serif;\n}\n\n.Chat_smallFont__lxXUD {\n    font-size: 14px;\n}\n\n.Chat_userImg__2_Mcb {\n    width: 50px;\n    height: 50px;\n    border-radius: 50%;\n    overflow: hidden;\n    margin-right: 10px;\n    flex-shrink: 0;\n}\n\n.Chat_userImg__2_Mcb img {\n    width: 100%;\n    height: 100%;\n    object-fit: cover;\n}\n\n.Chat_messageInput__296C1 {\n    width: 100%;\n    height: 40px;\n    display: flex;\n    transition: .3s;\n    justify-content: center;\n}\n\n.Chat_messageInput__296C1 input {\n    flex-grow: 1;\n    background: #4D5159;\n    padding: 0 10px 0 10px;\n    outline: none;\n    height: 100%;\n    color: #fff;\n    font-size: 14px;\n    font-family: 'Helvetica';\n    border: 0;\n}\n\n.Chat_messageInput__296C1 button {\n    flex-grow: 2;\n    width: 40px;\n    border: 1px solid #00FCB6;\n    flex-shrink: 0;\n    background: #4D5159;\n    outline: none;\n    cursor: pointer;\n    height: 100%;\n}\n\n.Chat_messageInput__296C1 button:hover svg {\n    transform: scale(1.2);\n}\n\n.Chat_messageInput__296C1 button:hover {\n    background: rgba(0, 252, 181, 0.198);\n}\n\n.Chat_messageInput__296C1 svg {\n    width: 50%;\n}\n\n", ""]);
// Exports
exports.locals = {
	"chatContainer": "Chat_chatContainer__1Qyks",
	"scrollBox": "Chat_scrollBox__27Cqx",
	"messageInput": "Chat_messageInput__296C1",
	"messageBox": "Chat_messageBox__TSLET",
	"smallFont": "Chat_smallFont__lxXUD",
	"userImg": "Chat_userImg__2_Mcb"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/GuestBoard.module.css":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/Meetup/GuestBoard/GuestBoard.module.css ***!
  \*******************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.GuestBoard_guestBoard__2eEoT {\n    right: 80px;\n    padding-bottom: 5%;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n}\n\n.GuestBoard_guestBoard__2eEoT h4 {\n    font-size: 18px;\n}\n\n.GuestBoard_guestList__x6BVD {\n    width: 100%;\n    max-height: -moz-fit-content;\n    max-height: fit-content;\n    display: flex;\n    flex-wrap: wrap;\n    transition: .5s;\n    justify-content: space-between;\n}\n\n.GuestBoard_memerImg__6t-gO {\n    margin-right: 5px;\n    margin-top: 10px;\n    width: 40px;\n    height: 40px;\n    border-radius: 50%;\n    overflow: hidden;\n}\n\n.GuestBoard_memerImg__6t-gO img {\n    width: 100%;\n    height: 100%;\n    object-fit: cover;\n}\n\n.GuestBoard_seeAllBtn__Iu7TH {\n    border: 1px solid #00FCB6;\n    color: #00FCB6;\n    font-size: 16px;\n    margin: 20px 0;\n    height: 40px;\n    width: 100%;\n    font-family: 'AribauBold';\n    cursor: pointer;\n    outline: none;\n    background: none;\n    transition: .3s;\n    flex-shrink: 0;\n}\n\n.GuestBoard_seeAllBtn__Iu7TH:hover {\n    background: rgba(0, 252, 181, 0.198);\n}\n\n.GuestBoard_bigFont__drzby {\n    font-size: 32px;\n}\n\n.GuestBoard_smallFont__41Ubu {\n    font-size: 14px;\n}", ""]);
// Exports
exports.locals = {
	"guestBoard": "GuestBoard_guestBoard__2eEoT",
	"guestList": "GuestBoard_guestList__x6BVD",
	"memerImg": "GuestBoard_memerImg__6t-gO",
	"seeAllBtn": "GuestBoard_seeAllBtn__Iu7TH",
	"bigFont": "GuestBoard_bigFont__drzby",
	"smallFont": "GuestBoard_smallFont__41Ubu"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/LeaderBoard/LeaderBoard.module.css":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/Meetup/LeaderBoard/LeaderBoard.module.css ***!
  \*********************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.LeaderBoard_leaderBoard__stPt8 {\n    left: 80px;\n    height: 100%;\n    padding-top: 50px;\n}\n\n.LeaderBoard_leaderSelector__1HZQA {\n    width: 75%;\n    display: flex;\n    justify-content: space-between;\n    align-items: center;\n    margin-bottom: 15px;\n}\n\n.LeaderBoard_leaderImg__3O0PJ {\n    width: 70px;\n    height: 70px;\n    border-radius: 50%;\n    overflow: hidden;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    cursor: pointer;\n}\n\n.LeaderBoard_leaderImg__3O0PJ img {\n    width: 100%;\n    flex-shrink: 0;\n}\n\n.LeaderBoard_infoBox__17-gG {\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    margin-top: 15px;\n}\n\n.LeaderBoard_infoBox__17-gG h3 {\n    font-size: 27px;\n}\n\n.LeaderBoard_topic__2Fca3 {\n    font-family: Arial, Helvetica, sans-serif;\n}\n\n.LeaderBoard_underline__1ni91 {\n    border-bottom: 1px solid #707070;\n}\n\n.LeaderBoard_infoBox__17-gG * {\n    margin-bottom: 15px;\n}\n\n.LeaderBoard_ambassadorImgBox__24hX9 {\n    width: 250px;\n    height: -moz-fit-content;\n    height: fit-content;\n    position: absolute;\n    bottom: 0;\n    left: 50px;\n}\n\n.LeaderBoard_ambassadorImg__2WLtR {\n    width: 100%;\n}\n\n.LeaderBoard_selected__2IyBs {\n    border: 2px solid #00FCB6;\n}", ""]);
// Exports
exports.locals = {
	"leaderBoard": "LeaderBoard_leaderBoard__stPt8",
	"leaderSelector": "LeaderBoard_leaderSelector__1HZQA",
	"leaderImg": "LeaderBoard_leaderImg__3O0PJ",
	"infoBox": "LeaderBoard_infoBox__17-gG",
	"topic": "LeaderBoard_topic__2Fca3",
	"underline": "LeaderBoard_underline__1ni91",
	"ambassadorImgBox": "LeaderBoard_ambassadorImgBox__24hX9",
	"ambassadorImg": "LeaderBoard_ambassadorImg__2WLtR",
	"selected": "LeaderBoard_selected__2IyBs"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Meetup.module.css":
/*!****************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/Meetup/Meetup.module.css ***!
  \****************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(/*! ../../assets/img/calendar.svg */ "./src/assets/img/calendar.svg");
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, ".Meetup_meetupContainer__2oj8D {\n    max-width: 700px;\n    margin: 0 auto;\n}\n\nheader {\n    color: #fff;\n    text-align: center;\n}\n\n.Meetup_title__12wIh {\n    font-size: 50px;\n}\n\n.Meetup_prompt__2SgAq {\n    color: #707070;\n    font-size: 20px;\n}\n\n.Meetup_date__3b6DV {\n    top: 10px;\n    font-size: 25px;\n    position: relative;\n    color: #707070;\n}\n\n.Meetup_date__3b6DV span {\n    color: #fff;\n}\n\n.Meetup_date__3b6DV::before {\n    position: absolute;\n    content: '';\n    width: 30px;\n    height: 30px;\n    left: -40px;\n    background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n    background-size: contain;\n}", ""]);
// Exports
exports.locals = {
	"meetupContainer": "Meetup_meetupContainer__2oj8D",
	"title": "Meetup_title__12wIh",
	"prompt": "Meetup_prompt__2SgAq",
	"date": "Meetup_date__3b6DV"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Seat/Seat.module.css":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/Meetup/Table/Seat/Seat.module.css ***!
  \*************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.Seat_seat__20Gki {\n    width: 100px;\n    height: 100px;\n    margin-bottom: 20px;\n}\n\nsvg {\n    width: 100%;\n    height: 100%;\n    transition: .3s;\n    cursor: pointer;\n}\n\n.Seat_seat__20Gki svg:hover {\n    transform: scale(1.1);\n}\n\n.Seat_right__2QmT- {\n    transform: rotate(180deg);\n}", ""]);
// Exports
exports.locals = {
	"seat": "Seat_seat__20Gki",
	"right": "Seat_right__2QmT-"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Table.module.css":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/Meetup/Table/Table.module.css ***!
  \*********************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(/*! ../../../assets/img/megaphone.svg */ "./src/assets/img/megaphone.svg");
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "\n.Table_tableContainer__3gOoC {\n    width: 60%;\n    padding-top: 50px;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    margin: 0 auto;\n}\n\n.Table_spaceBetween__3JCcW {\n    display: flex;\n    width: 100%;\n    justify-content: space-between;\n}\n\n.Table_table__3zHts {\n    width: 100%;\n    margin-left: 20px;\n    margin-right: 20px;\n    border-radius: 10px;\n    border: 2px solid #00FAB5;\n}\n\n.Table_speakerBox__1PrL0 {\n    width: -moz-fit-content;\n    width: fit-content;\n    display: flex;\n    flex-direction: column;\n    align-items: center;\n    color: #fff;\n    margin-bottom: 20px;\n}\n\n.Table_speakerImg__VA-rA {\n    width: 90px;\n    height: 90px;\n    margin: 10px 0;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    border-radius: 50%;\n    overflow: hidden;\n    border: 2px solid #00FAB5;\n}\n\n.Table_speakerType__1zcH0 {\n    position: relative;\n}\n\n.Table_speakerType__1zcH0::before {\n    position: absolute;\n    content: '';\n    width: 23px;\n    height: 23px;\n    left: -28px;\n    background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n    background-size: contain;\n}\n\n.Table_speakerImg__VA-rA img {\n    width: 100%;\n}", ""]);
// Exports
exports.locals = {
	"tableContainer": "Table_tableContainer__3gOoC",
	"spaceBetween": "Table_spaceBetween__3JCcW",
	"table": "Table_table__3zHts",
	"speakerBox": "Table_speakerBox__1PrL0",
	"speakerImg": "Table_speakerImg__VA-rA",
	"speakerType": "Table_speakerType__1zcH0"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/Menu/Menu.module.css":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/common/Menu/Menu.module.css ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(/*! ../../../assets/img/logo_arrow.svg */ "./src/assets/img/logo_arrow.svg");
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "\n.Menu_menu__27czC {\n    height: 100vh;\n    background: rgba(0, 0, 0, 0.4);\n    position: fixed;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    flex-direction: column;\n    transition: .5s;\n    left: 0;\n    top: 0;\n    z-index: 10;\n}\n\n.Menu_closedMenu__3Q2sd {\n    width: 80px;\n}\n\n.Menu_openedMenu__1k9Li {\n    width: 330px;\n}\n\n.Menu_logo___Mlk5 {\n    position: absolute;\n    left: 20%;\n    top: 50px;\n}\n\n.Menu_closedMenu__3Q2sd .Menu_logo___Mlk5 {\n    width: 60%;\n}\n\n.Menu_openedMenu__1k9Li .Menu_logo___Mlk5 {\n    width: 40%;\n}\n\n.Menu_menuButton__2vXjV {\n    transform: rotate(90deg);\n    background: 0;\n    color: #707070;\n    font-family: 'AribauBold';\n    font-size: 30px;\n    outline: none;\n    cursor: pointer;\n    transition: .3s;\n    border: 0;\n}\n\n.Menu_menuButton__2vXjV:hover {\n    color: #adacac;\n}\n\n.Menu_openedLogo__VtHhO {\n    position: absolute;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    left: 20%;\n    top: 50px;\n}\n\n.Menu_logo___Mlk5 .Menu_logoImg__eRjtK {\n    transition: .3s;\n    flex-shrink: 0;\n    width: 100px;\n    display: flex;\n    justify-content: center;\n}\n\n.Menu_logo___Mlk5::before {\n    position: absolute;\n    content: '';\n    width: 25px;\n    height: 25px;\n    top: 10px;\n    left: -30px;\n    background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat;\n    background-size: contain;\n    transform: rotate(180deg);\n}\n\n.Menu_logo___Mlk5::after {\n    position: absolute;\n    content: '';\n    width: 25px;\n    height: 25px;\n    top: 10px;\n    right: -30px;\n    background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat;\n    background-size: contain;\n}\n\n.Menu_logoImg__eRjtK img {\n    width: 100%;\n}\n\n.Menu_closedLogo__3l5xj .Menu_logoImg__eRjtK {\n    width: 0px;\n    overflow: hidden;\n}", ""]);
// Exports
exports.locals = {
	"menu": "Menu_menu__27czC",
	"closedMenu": "Menu_closedMenu__3Q2sd",
	"openedMenu": "Menu_openedMenu__1k9Li",
	"logo": "Menu_logo___Mlk5",
	"menuButton": "Menu_menuButton__2vXjV",
	"openedLogo": "Menu_openedLogo__VtHhO",
	"logoImg": "Menu_logoImg__eRjtK",
	"closedLogo": "Menu_closedLogo__3l5xj"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/PastEvents/PastEvents.module.css":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/common/PastEvents/PastEvents.module.css ***!
  \*******************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
exports = ___CSS_LOADER_API_IMPORT___(false);
// Module
exports.push([module.i, "\n.PastEvents_pastEvents__H2mgw {\n    width: 80px;\n    height: 100vh;\n    background: rgba(0, 0, 0, 0.4);\n    position: fixed;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    flex-direction: column;\n    transition: .3s;\n    right: 0;\n    top: 0;\n}\n\n.PastEvents_pastEventsButton__Z4T3F {\n    background: 0;\n    color: #707070;\n    font-family: 'AribauBold';\n    font-size: 30px;\n    outline: none;\n    cursor: pointer;\n    border: 0;\n    white-space: nowrap;\n    transition: .3s;\n    transform: rotate(-90deg);\n}\n\n.PastEvents_pastEventsButton__Z4T3F:hover {\n    color: #adacac;\n}", ""]);
// Exports
exports.locals = {
	"pastEvents": "PastEvents_pastEvents__H2mgw",
	"pastEventsButton": "PastEvents_pastEventsButton__Z4T3F"
};
module.exports = exports;


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css":
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!./node_modules/postcss-loader/src??postcss!./src/index.css ***!
  \**************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(/*! ../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(/*! ./assets/img/background.jpg */ "./src/assets/img/background.jpg");
var ___CSS_LOADER_URL_IMPORT_1___ = __webpack_require__(/*! ./assets/fonts/AribauGroteskExtraLight.otf */ "./src/assets/fonts/AribauGroteskExtraLight.otf");
var ___CSS_LOADER_URL_IMPORT_2___ = __webpack_require__(/*! ./assets/fonts/AribauGroteskBold.otf */ "./src/assets/fonts/AribauGroteskBold.otf");
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
var ___CSS_LOADER_URL_REPLACEMENT_1___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_1___);
var ___CSS_LOADER_URL_REPLACEMENT_2___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_2___);
// Module
exports.push([module.i, "body {\n  margin: 0;\n  margin-top: 40px;\n  width: 100vw;\n  height: 100vh;\n  font-family: 'AribauBold', Arial, Helvetica, sans-serif;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ");\n}\n\ncode {\n  font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',\n    monospace;\n}\n\nh1, h2, h3, h4, h5, h6, p, ul, span {\n  margin: 0;\n  padding: 0;\n  color: #fff;\n}\n\nul {\n  list-style: none;\n}\n\n@font-face{\n  font-family: 'AribauExtraLight';\n  src: url(" + ___CSS_LOADER_URL_REPLACEMENT_1___ + ");\n  font-style: normal;\n  font-weight: 300;\n}\n\n@font-face{\n  font-family: 'AribauBold';\n  src: url(" + ___CSS_LOADER_URL_REPLACEMENT_2___ + ");\n  font-style: normal;\n  font-weight: 300;\n}\n\n.sideboard {\n  width: 210px;\n  padding: 50px 20px 0px 20px;\n  position: fixed;\n  height: 90vh;\n  padding-top: 5vh;\n  padding-bottom: 5vh;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  top: 0px;\n  background: rgba(255, 255, 255, 0.15);\n}\n\n.grey {\n  color: #707070;\n}\n\n.selected {\n    border: 1px solid #00FCB6;\n}", ""]);
// Exports
module.exports = exports;


/***/ }),

/***/ "./src/App.css":
/*!*********************!*\
  !*** ./src/App.css ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./App.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/App.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/App.js":
/*!********************!*\
  !*** ./src/App.js ***!
  \********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var _components_Meetup_MeetupContainer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/Meetup/MeetupContainer */ "./src/components/Meetup/MeetupContainer.js");
/* harmony import */ var _components_Adgenda_AdgendaContainer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/Adgenda/AdgendaContainer */ "./src/components/Adgenda/AdgendaContainer.js");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./App.css */ "./src/App.css");
/* harmony import */ var _App_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_App_css__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/App.js";






function App() {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: "app-wrapper",
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    path: ":id?",
    render: () => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Meetup_MeetupContainer__WEBPACK_IMPORTED_MODULE_2__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 11,
        columnNumber: 25
      }
    }),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 7
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_1__["Route"], {
    path: "/adgenda/:id?",
    render: () => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Adgenda_AdgendaContainer__WEBPACK_IMPORTED_MODULE_3__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 14,
        columnNumber: 23
      }
    }),
    __self: this,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 7
    }
  }));
}

/* harmony default export */ __webpack_exports__["default"] = (App);

/***/ }),

/***/ "./src/assets/fonts/AribauGroteskBold.otf":
/*!************************************************!*\
  !*** ./src/assets/fonts/AribauGroteskBold.otf ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/AribauGroteskBold.702c5c71.otf";

/***/ }),

/***/ "./src/assets/fonts/AribauGroteskExtraLight.otf":
/*!******************************************************!*\
  !*** ./src/assets/fonts/AribauGroteskExtraLight.otf ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/AribauGroteskExtraLight.0410984d.otf";

/***/ }),

/***/ "./src/assets/img/background.jpg":
/*!***************************************!*\
  !*** ./src/assets/img/background.jpg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/background.44a71717.jpg";

/***/ }),

/***/ "./src/assets/img/calendar.svg":
/*!*************************************!*\
  !*** ./src/assets/img/calendar.svg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/calendar.c0f1b82c.svg";

/***/ }),

/***/ "./src/assets/img/face.jpeg":
/*!**********************************!*\
  !*** ./src/assets/img/face.jpeg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/face.3eccedb4.jpeg";

/***/ }),

/***/ "./src/assets/img/geekle.svg":
/*!***********************************!*\
  !*** ./src/assets/img/geekle.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/geekle.393163c5.svg";

/***/ }),

/***/ "./src/assets/img/logo_arrow.svg":
/*!***************************************!*\
  !*** ./src/assets/img/logo_arrow.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo_arrow.477c3dac.svg";

/***/ }),

/***/ "./src/assets/img/logo_max.svg":
/*!*************************************!*\
  !*** ./src/assets/img/logo_max.svg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo_max.0ca8ace7.svg";

/***/ }),

/***/ "./src/assets/img/logo_min.svg":
/*!*************************************!*\
  !*** ./src/assets/img/logo_min.svg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/logo_min.ebdc6835.svg";

/***/ }),

/***/ "./src/assets/img/megaphone.svg":
/*!**************************************!*\
  !*** ./src/assets/img/megaphone.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/media/megaphone.3b822210.svg";

/***/ }),

/***/ "./src/components/Adgenda/Adgenda.jsx":
/*!********************************************!*\
  !*** ./src/components/Adgenda/Adgenda.jsx ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./src/components/Adgenda/AdgendaContainer.js":
/*!****************************************************!*\
  !*** ./src/components/Adgenda/AdgendaContainer.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Adgenda__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Adgenda */ "./src/components/Adgenda/Adgenda.jsx");
/* harmony import */ var _Adgenda__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Adgenda__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Adgenda/AdgendaContainer.js";



class AdgendaContainer extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  componentDidMount() {}

  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Adgenda__WEBPACK_IMPORTED_MODULE_1___default.a, {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 7
      }
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (AdgendaContainer);

/***/ }),

/***/ "./src/components/Meetup/GuestBoard/Chat/Chat.jsx":
/*!********************************************************!*\
  !*** ./src/components/Meetup/GuestBoard/Chat/Chat.jsx ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Chat_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Chat.module.css */ "./src/components/Meetup/GuestBoard/Chat/Chat.module.css");
/* harmony import */ var _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Chat_module_css__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/GuestBoard/Chat/Chat.jsx";



const Chat = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.chatContainer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.scrollBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.messageBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.userImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.desription,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 25
    }
  }, "Name Surname"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.smallFont + ' grey',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 25
    }
  }, "Tech Speaker"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 25
    }
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.messageBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.userImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.desription,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 25
    }
  }, "Name Surname"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.smallFont + ' grey',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 25
    }
  }, "Tech Speaker"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 25
    }
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.messageBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.userImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.desription,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 25
    }
  }, "Name Surname"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.smallFont + ' grey',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 25
    }
  }, "Tech Speaker"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 25
    }
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor."))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.messageBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.userImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.desription,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 25
    }
  }, "Name Surname"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.smallFont + ' grey',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45,
      columnNumber: 25
    }
  }, "Tech Speaker"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46,
      columnNumber: 25
    }
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.")))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Chat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.messageInput,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
    type: "text",
    placeholder: "message",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51,
      columnNumber: 17
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "25.051",
    height: "25.048",
    viewBox: "0 0 25.051 25.048",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 53,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", {
    d: "M88.3,64.1,64.347,74.535a.549.549,0,0,0,.02,1l6.478,3.66a1.045,1.045,0,0,0,1.194-.117L84.813,68.063c.085-.072.287-.209.365-.13s-.046.281-.117.365L74.009,80.746a1.041,1.041,0,0,0-.1,1.246l4.234,6.792a.551.551,0,0,0,.992-.013l9.9-23.95A.549.549,0,0,0,88.3,64.1Z",
    transform: "translate(-64.036 -64.04)",
    fill: "#00fcb6",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 54,
      columnNumber: 25
    }
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (Chat);

/***/ }),

/***/ "./src/components/Meetup/GuestBoard/Chat/Chat.module.css":
/*!***************************************************************!*\
  !*** ./src/components/Meetup/GuestBoard/Chat/Chat.module.css ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../../node_modules/postcss-loader/src??postcss!./Chat.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/Chat/Chat.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../../node_modules/postcss-loader/src??postcss!./Chat.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/Chat/Chat.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../../node_modules/postcss-loader/src??postcss!./Chat.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/Chat/Chat.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/Meetup/GuestBoard/Chat/ChatContainer.js":
/*!****************************************************************!*\
  !*** ./src/components/Meetup/GuestBoard/Chat/ChatContainer.js ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Chat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Chat */ "./src/components/Meetup/GuestBoard/Chat/Chat.jsx");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/GuestBoard/Chat/ChatContainer.js";



const ChatContainer = props => {
  const messages = {};
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Chat__WEBPACK_IMPORTED_MODULE_1__["default"], {
    messages: messages,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 7
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (ChatContainer);

/***/ }),

/***/ "./src/components/Meetup/GuestBoard/GuestBoard.jsx":
/*!*********************************************************!*\
  !*** ./src/components/Meetup/GuestBoard/GuestBoard.jsx ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./GuestBoard.module.css */ "./src/components/Meetup/GuestBoard/GuestBoard.module.css");
/* harmony import */ var _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Chat_ChatContainer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Chat/ChatContainer */ "./src/components/Meetup/GuestBoard/Chat/ChatContainer.js");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/GuestBoard/GuestBoard.jsx";




const GuestBoard = props => {
  const [members, setMembers] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(props.members.filter((m, index) => index < 8));
  const [isOpen, setIsOpen] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    isOpen ? setMembers(props.members) : setMembers(props.members.filter((m, index) => index < 8));
  }, [isOpen]);

  const getAllMembers = () => {
    setIsOpen(!isOpen);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.guestBoard + ' ' + 'sideboard',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 13
    }
  }, "Java Geekle Amsterdam "), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.bigFont,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 13
    }
  }, "245"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.smallFont,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 13
    }
  }, "members"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.guestList,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 13
    }
  }, members.map(member => /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    key: member.id,
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.memerImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 25
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: member.photo,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 29
    }
  })))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    onClick: () => getAllMembers(),
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.seeAllBtn,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 13
    }
  }, isOpen ? 'Close' : 'See all'), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 13
    }
  }, "Chat"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.bigFont,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 13
    }
  }, "9"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _GuestBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.smallFont,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 13
    }
  }, "Online"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Chat_ChatContainer__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 13
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (GuestBoard);

/***/ }),

/***/ "./src/components/Meetup/GuestBoard/GuestBoard.module.css":
/*!****************************************************************!*\
  !*** ./src/components/Meetup/GuestBoard/GuestBoard.module.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./GuestBoard.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/GuestBoard.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./GuestBoard.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/GuestBoard.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./GuestBoard.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/GuestBoard/GuestBoard.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/Meetup/GuestBoard/GuestBoardContainer.js":
/*!*****************************************************************!*\
  !*** ./src/components/Meetup/GuestBoard/GuestBoardContainer.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _GuestBoard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./GuestBoard */ "./src/components/Meetup/GuestBoard/GuestBoard.jsx");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/GuestBoard/GuestBoardContainer.js";



const GuestBoardContainer = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_GuestBoard__WEBPACK_IMPORTED_MODULE_1__["default"], {
    members: props.members,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (GuestBoardContainer);

/***/ }),

/***/ "./src/components/Meetup/LeaderBoard/LeaderBoard.jsx":
/*!***********************************************************!*\
  !*** ./src/components/Meetup/LeaderBoard/LeaderBoard.jsx ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LeaderBoard.module.css */ "./src/components/Meetup/LeaderBoard/LeaderBoard.module.css");
/* harmony import */ var _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! d3 */ "./node_modules/d3/index.js");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/LeaderBoard/LeaderBoard.jsx";




const LeaderBoard = props => {
  const [selectedLeader, setLeader] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(props.leaders[0]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {}, [selectedLeader]);

  const selectLeader = id => {
    props.leaders.forEach(leader => leader.id === id ? setLeader(leader) : null);
  };

  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.leaderBoard + ' ' + 'sideboard',
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.leaderSelector,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 13
    }
  }, props.leaders && props.leaders.map(leader => {
    let classes = `${selectedLeader.id === leader.id ? _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.selected : null} ${_LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.leaderImg}`;
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
      key: leader.id,
      onClick: () => selectLeader(leader.id),
      className: classes,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 29
      }
    }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
      src: leader.photo,
      __self: undefined,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 33
      }
    }));
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "grey",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 13
    }
  }, "Choose a leader"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.infoBox + ' ' + _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.underline,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 17
    }
  }, selectedLeader.firstName, " ", selectedLeader.lastName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "grey",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 17
    }
  }, selectedLeader.type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 17
    }
  }, "Topic:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.topic,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 38,
      columnNumber: 17
    }
  }, selectedLeader.topic)), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.infoBox + ' ' + _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.ambassadorBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h3", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 17
    }
  }, props.ambassador.firstName, " ", props.ambassador.lastName), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: "grey",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 17
    }
  }, props.ambassador.type), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.ambassadorImgBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    className: _LeaderBoard_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.ambassadorImg,
    src: props.ambassador.img,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44,
      columnNumber: 21
    }
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (LeaderBoard);

/***/ }),

/***/ "./src/components/Meetup/LeaderBoard/LeaderBoard.module.css":
/*!******************************************************************!*\
  !*** ./src/components/Meetup/LeaderBoard/LeaderBoard.module.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./LeaderBoard.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/LeaderBoard/LeaderBoard.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./LeaderBoard.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/LeaderBoard/LeaderBoard.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./LeaderBoard.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/LeaderBoard/LeaderBoard.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/Meetup/LeaderBoard/LeaderBoardContainer.js":
/*!*******************************************************************!*\
  !*** ./src/components/Meetup/LeaderBoard/LeaderBoardContainer.js ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _LeaderBoard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./LeaderBoard */ "./src/components/Meetup/LeaderBoard/LeaderBoard.jsx");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/LeaderBoard/LeaderBoardContainer.js";



const LeaderBoardContainer = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_LeaderBoard__WEBPACK_IMPORTED_MODULE_1__["default"], {
    leaders: props.leaders,
    ambassador: props.ambassador,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 7
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (LeaderBoardContainer);

/***/ }),

/***/ "./src/components/Meetup/Meetup.jsx":
/*!******************************************!*\
  !*** ./src/components/Meetup/Meetup.jsx ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Meetup_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Meetup.module.css */ "./src/components/Meetup/Meetup.module.css");
/* harmony import */ var _Meetup_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Meetup_module_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Table_Table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Table/Table */ "./src/components/Meetup/Table/Table.jsx");
/* harmony import */ var _assets_img_face_jpeg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../assets/img/face.jpeg */ "./src/assets/img/face.jpeg");
/* harmony import */ var _assets_img_face_jpeg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_img_face_jpeg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _common_Menu_Menu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/Menu/Menu */ "./src/components/common/Menu/Menu.jsx");
/* harmony import */ var _common_PastEvents_PastEventsContainer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/PastEvents/PastEventsContainer */ "./src/components/common/PastEvents/PastEventsContainer.js");
/* harmony import */ var _LeaderBoard_LeaderBoardContainer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./LeaderBoard/LeaderBoardContainer */ "./src/components/Meetup/LeaderBoard/LeaderBoardContainer.js");
/* harmony import */ var _GuestBoard_GuestBoardContainer__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./GuestBoard/GuestBoardContainer */ "./src/components/Meetup/GuestBoard/GuestBoardContainer.js");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/Meetup.jsx";









const Meetup = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Meetup_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.meetupContainer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 5
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("header", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
    className: _Meetup_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.title,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 13
    }
  }, "Phython Geekle Meetup"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
    className: _Meetup_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.prompt,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 13
    }
  }, "To become a participant in the event, select a place"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("time", {
    className: _Meetup_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.date,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 17
    }
  }, "21 "), "th of", /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 17
    }
  }, " March"))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_Menu_Menu__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_LeaderBoard_LeaderBoardContainer__WEBPACK_IMPORTED_MODULE_6__["default"], {
    leaders: props.leaders,
    ambassador: props.ambassador,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 23,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Table_Table__WEBPACK_IMPORTED_MODULE_2__["default"], {
    speakerImg: _assets_img_face_jpeg__WEBPACK_IMPORTED_MODULE_3___default.a,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_GuestBoard_GuestBoardContainer__WEBPACK_IMPORTED_MODULE_7__["default"], {
    members: props.members,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_PastEvents_PastEventsContainer__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 9
    }
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Meetup);

/***/ }),

/***/ "./src/components/Meetup/Meetup.module.css":
/*!*************************************************!*\
  !*** ./src/components/Meetup/Meetup.module.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../node_modules/postcss-loader/src??postcss!./Meetup.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Meetup.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../node_modules/postcss-loader/src??postcss!./Meetup.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Meetup.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../node_modules/postcss-loader/src??postcss!./Meetup.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Meetup.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/Meetup/MeetupContainer.js":
/*!**************************************************!*\
  !*** ./src/components/Meetup/MeetupContainer.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Meetup__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Meetup */ "./src/components/Meetup/Meetup.jsx");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/MeetupContainer.js";



const MeetupContainer = props => {
  const leaders = [{
    id: 'sfd324fo3fu0f03',
    firstName: 'User1',
    lastName: 'Surname',
    type: 'Soft trainer',
    topic: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://sun9-57.userapi.com/c858120/v858120187/104218/eFVW5kTajOo.jpg?ava=1'
  }, {
    id: 'sfd324fo3fuasdfaq',
    firstName: 'User2',
    lastName: 'Surname',
    type: 'Tech speaker',
    topic: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://image.ebdcdn.com/image/upload/c_fill,e_sharpen:70,f_auto,h_600,q_auto:good,w_400/v1/product/model/portrait/pm0477_w1.jpg'
  }];
  const members = [{
    id: 'sfd324foasdf3fusada12345s',
    firstName: 'Guest1',
    lastName: 'LastName',
    interests: ['Java', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://lh3.googleusercontent.com/proxy/bnMycr_ojbMVhw20TO2axtFPwPQ03-QpiZkumi_oJrP09C6x5_55e4vW_z0n4ISMHE27k9PBAGN_gtUow40eeoEAHZa5g4EybL-_uEJ0SJ3tzOuo2c0bTHkXSIpt0JyKkyOrgAd5Yai-v-fvR1U6dzsudI-4XntrJRfjFaDch0A4Ft7SdwjFaoMQ3sikFepEfG2_amX203-qG1s'
  }, {
    id: 'sfd324rw24rr2df3fusaenbmujjydas',
    firstName: 'Guest2',
    lastName: 'LastName',
    interests: ['Java', 'Minecraft', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg'
  }, {
    id: 'sfwq4t32eg254gasdf3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfd324rw24rr2df3fus123y75adas',
    firstName: 'Guest2',
    lastName: 'LastName',
    interests: ['Java', 'Minecraft', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg'
  }, {
    id: 'sfwq4t324gasdf3fusa23621das',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfd324rw24rr2df3fusa125312das',
    firstName: 'Guest2',
    lastName: 'LastName',
    interests: ['Java', 'Minecraft', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg'
  }, {
    id: 'sfwq4t324gasdf3fusa512435das',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfd324rw242345rr2df3fusadas',
    firstName: 'Guest2',
    lastName: 'LastName',
    interests: ['Java', 'Minecraft', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg'
  }, {
    id: 's123425fwq4t324gasdf3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324gasdf3fu1234sadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324gasd65324f3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324gasd2134f3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t3425324gasdf3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324gasg13aszxdf3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324weqrfvdsvgasdf3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324gasdf3234f2qefusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }, {
    id: 'sfwq4t324g234t3vr3asdf3fusadas',
    firstName: 'Guest3',
    lastName: 'LastName',
    interests: ['Node.js', 'C++', 'React.js'],
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.',
    photo: 'https://i.pinimg.com/736x/c0/a1/0e/c0a10edf16e14220833f9319aa70783e.jpg'
  }];
  const ambassador = {
    id: '21rfdfwf423fsdasdfasdff2e',
    firstName: 'Mila',
    lastName: 'Surname',
    type: 'Ambassador',
    img: 'https://www.pngarts.com/files/3/Man-PNG-Free-Download.png'
  };
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Meetup__WEBPACK_IMPORTED_MODULE_1__["default"], {
    leaders: leaders,
    ambassador: ambassador,
    members: members,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 172,
      columnNumber: 7
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (MeetupContainer);

/***/ }),

/***/ "./src/components/Meetup/Table/Seat/Seat.jsx":
/*!***************************************************!*\
  !*** ./src/components/Meetup/Table/Seat/Seat.jsx ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Seat_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Seat.module.css */ "./src/components/Meetup/Table/Seat/Seat.module.css");
/* harmony import */ var _Seat_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Seat_module_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var d3__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! d3 */ "./node_modules/d3/index.js");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/Table/Seat/Seat.jsx";




const Seat = props => {
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {}, []);
  const direction = props.isLeft ? '' : _Seat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.right;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Seat_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.seat + ' ' + direction,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    width: "127.179",
    height: "142.461",
    viewBox: "0 0 127.179 142.461",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("g", {
    transform: "translate(-731.157 -601.24)",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", {
    fill: "#fff",
    d: "M2110.329 817.917c-20.678 27.131-37.022 34.483-59.107 26.582-18.912-6.765-31.458-24.861-30.637-44.184.769-18.02 14.793-37.49 30.009-41.66 26.34-7.216 40.378-.849 60.068 28.582 11.153-7.6 13.56-15.823 4.831-25.33-7.149-7.786-15.238-14.717-23.045-21.885-14.25-13.083-49.378-11.714-62.474 2.34-26.018 27.931-28.749 81.853-5.7 112.617 12.962 17.308 49.3 24.6 64.035 12.371a339.529 339.529 0 0026.573-24.761c9.342-9.631 7.681-17.129-4.553-24.672z",
    opacity: "0.43",
    transform: "translate(-1262.527 -129.728)",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 21
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", {
    fill: "#fff",
    d: "M2055.429 741.526c-23.176-.685-41.169 16.387-41.144 39.038a40.144 40.144 0 0080.283.376c.287-22.054-16.282-38.74-39.139-39.414z",
    opacity: "0.43",
    transform: "translate(-1250.957 -108.33)",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 21
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", {
    fill: "#fff",
    d: "M2025.911 734.337c-21.021 19.606-23.446 44.6-21.839 70.2 1.258 20.106 5.885 39.505 23.748 53.376-21.415-41.099-20.12-82.013-1.909-123.576z",
    opacity: "0.43",
    transform: "translate(-1272.512 -122.888)",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 21
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("g", {
    className: "plus",
    transform: "translate(785.794 654.119)",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("path", {
    fill: "#00fcb6",
    d: "M183 164.889h-12.669v-12.668a2.721 2.721 0 10-5.442 0v12.669h-12.668a2.606 2.606 0 00-2.721 2.721 2.634 2.634 0 002.721 2.721h12.669V183a2.635 2.635 0 002.721 2.721 2.707 2.707 0 002.72-2.721v-12.669H183a2.721 2.721 0 100-5.442z",
    transform: "translate(-149.5 -149.5)",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 25
    }
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (Seat);

/***/ }),

/***/ "./src/components/Meetup/Table/Seat/Seat.module.css":
/*!**********************************************************!*\
  !*** ./src/components/Meetup/Table/Seat/Seat.module.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../../node_modules/postcss-loader/src??postcss!./Seat.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Seat/Seat.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../../node_modules/postcss-loader/src??postcss!./Seat.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Seat/Seat.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../../node_modules/postcss-loader/src??postcss!./Seat.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Seat/Seat.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/Meetup/Table/Table.jsx":
/*!***********************************************!*\
  !*** ./src/components/Meetup/Table/Table.jsx ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Table_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Table.module.css */ "./src/components/Meetup/Table/Table.module.css");
/* harmony import */ var _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Table_module_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _Seat_Seat__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Seat/Seat */ "./src/components/Meetup/Table/Seat/Seat.jsx");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/Meetup/Table/Table.jsx";




const Table = props => {
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.tableContainer,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.speakerBox,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    className: _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.speakerType,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10,
      columnNumber: 17
    }
  }, "Tech Speaker:"), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.speakerImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: props.speakerImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 21
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14,
      columnNumber: 17
    }
  }, "Name Surname")), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.spaceBetween,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "seats",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: true,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 25
    }
  }))), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Table_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.table,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31,
      columnNumber: 17
    }
  }), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
    className: "seats",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: false,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: false,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: false,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 25
    }
  })), /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42,
      columnNumber: 21
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_Seat_Seat__WEBPACK_IMPORTED_MODULE_2__["default"], {
    isLeft: false,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43,
      columnNumber: 25
    }
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (Table);

/***/ }),

/***/ "./src/components/Meetup/Table/Table.module.css":
/*!******************************************************!*\
  !*** ./src/components/Meetup/Table/Table.module.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./Table.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Table.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./Table.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Table.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./Table.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/Meetup/Table/Table.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/common/Menu/Menu.jsx":
/*!*********************************************!*\
  !*** ./src/components/common/Menu/Menu.jsx ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Menu_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Menu.module.css */ "./src/components/common/Menu/Menu.module.css");
/* harmony import */ var _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_Menu_module_css__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _assets_img_logo_min_svg__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../assets/img/logo_min.svg */ "./src/assets/img/logo_min.svg");
/* harmony import */ var _assets_img_logo_min_svg__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logo_min_svg__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _assets_img_logo_max_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../assets/img/logo_max.svg */ "./src/assets/img/logo_max.svg");
/* harmony import */ var _assets_img_logo_max_svg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_assets_img_logo_max_svg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _assets_img_geekle_svg__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../assets/img/geekle.svg */ "./src/assets/img/geekle.svg");
/* harmony import */ var _assets_img_geekle_svg__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_assets_img_geekle_svg__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/common/Menu/Menu.jsx";






const Menu = props => {
  const [menuIsOpen, setMenu] = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {}, [menuIsOpen]);

  const toggleMenu = () => {
    setMenu(!menuIsOpen);
  };

  const menuStyle = `${menuIsOpen ? _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.openedMenu : _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.closedMenu} ${_Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.menu}`;
  const logoStyle = `${menuIsOpen ? _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.openedLogo : _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.closedLogo} ${_Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.logo}`;
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: menuStyle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: logoStyle,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.logoImg,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 17
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: _assets_img_geekle_svg__WEBPACK_IMPORTED_MODULE_4___default.a,
    alt: "geekle",
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27,
      columnNumber: 21
    }
  }))), !menuIsOpen ? /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    onClick: () => toggleMenu(),
    className: _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.menuButton,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35,
      columnNumber: 25
    }
  }, "Menu")) : /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    onClick: () => toggleMenu(),
    className: _Menu_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.menuButton,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40,
      columnNumber: 25
    }
  }, "Menu")));
};

/* harmony default export */ __webpack_exports__["default"] = (Menu);

/***/ }),

/***/ "./src/components/common/Menu/Menu.module.css":
/*!****************************************************!*\
  !*** ./src/components/common/Menu/Menu.module.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./Menu.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/Menu/Menu.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./Menu.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/Menu/Menu.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./Menu.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/Menu/Menu.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/common/PastEvents/PastEvents.jsx":
/*!*********************************************************!*\
  !*** ./src/components/common/PastEvents/PastEvents.jsx ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PastEvents_module_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PastEvents.module.css */ "./src/components/common/PastEvents/PastEvents.module.css");
/* harmony import */ var _PastEvents_module_css__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_PastEvents_module_css__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/common/PastEvents/PastEvents.jsx";



const PastEvents = props => {
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {}, []);
  return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    className: _PastEvents_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.pastEvents,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11,
      columnNumber: 9
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 13
    }
  }, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
    className: _PastEvents_module_css__WEBPACK_IMPORTED_MODULE_1___default.a.pastEventsButton,
    __self: undefined,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13,
      columnNumber: 17
    }
  }, "Past Events")));
};

/* harmony default export */ __webpack_exports__["default"] = (PastEvents);

/***/ }),

/***/ "./src/components/common/PastEvents/PastEvents.module.css":
/*!****************************************************************!*\
  !*** ./src/components/common/PastEvents/PastEvents.module.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./PastEvents.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/PastEvents/PastEvents.module.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./PastEvents.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/PastEvents/PastEvents.module.css", function() {
		var newContent = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!../../../../node_modules/postcss-loader/src??postcss!./PastEvents.module.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/PastEvents/PastEvents.module.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/components/common/PastEvents/PastEventsContainer.js":
/*!*****************************************************************!*\
  !*** ./src/components/common/PastEvents/PastEventsContainer.js ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PastEvents__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PastEvents */ "./src/components/common/PastEvents/PastEvents.jsx");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/components/common/PastEvents/PastEventsContainer.js";



class PastEventsContainer extends react__WEBPACK_IMPORTED_MODULE_0___default.a.Component {
  componentDidMount() {}

  render() {
    return /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_PastEvents__WEBPACK_IMPORTED_MODULE_1__["default"], {
      __self: this,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 12,
        columnNumber: 7
      }
    });
  }

}

/* harmony default export */ __webpack_exports__["default"] = (PastEventsContainer);

/***/ }),

/***/ "./src/index.css":
/*!***********************!*\
  !*** ./src/index.css ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(true) {
	module.hot.accept(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css", function() {
		var newContent = __webpack_require__(/*! !../node_modules/css-loader/dist/cjs.js??ref--6-oneOf-3-1!../node_modules/postcss-loader/src??postcss!./index.css */ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/index.css");

		if(typeof newContent === 'string') newContent = [[module.i, newContent, '']];

		var locals = (function(a, b) {
			var key, idx = 0;

			for(key in a) {
				if(!b || a[key] !== b[key]) return false;
				idx++;
			}

			for(key in b) idx--;

			return idx === 0;
		}(content.locals, newContent.locals));

		if(!locals) throw new Error('Aborting CSS HMR due to changed css-modules locals.');

		update(newContent);
	});

	module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.css */ "./src/index.css");
/* harmony import */ var _index_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_index_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _App__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./App */ "./src/App.js");
/* harmony import */ var _serviceWorker__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./serviceWorker */ "./src/serviceWorker.js");
/* harmony import */ var react_router_dom__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/esm/react-router-dom.js");
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");
var _jsxFileName = "/Users/vladkalinichenko/Documents/Diploma/diploma/src/index.js";







react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render( /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_router_dom__WEBPACK_IMPORTED_MODULE_5__["BrowserRouter"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 10,
    columnNumber: 3
  }
}, /*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_App__WEBPACK_IMPORTED_MODULE_3__["default"], {
  __self: undefined,
  __source: {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 7
  }
})), document.getElementById('root')); // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

_serviceWorker__WEBPACK_IMPORTED_MODULE_4__["unregister"]();

/***/ }),

/***/ "./src/serviceWorker.js":
/*!******************************!*\
  !*** ./src/serviceWorker.js ***!
  \******************************/
/*! exports provided: register, unregister */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "register", function() { return register; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unregister", function() { return unregister; });
// This optional code is used to register a service worker.
// register() is not called by default.
// This lets the app load faster on subsequent visits in production, and gives
// it offline capabilities. However, it also means that developers (and users)
// will only see deployed updates on subsequent visits to a page, after all the
// existing tabs open on the page have been closed, since previously cached
// resources are updated in the background.
// To learn more about the benefits of this model and instructions on how to
// opt-in, read https://bit.ly/CRA-PWA
const isLocalhost = Boolean(window.location.hostname === 'localhost' || // [::1] is the IPv6 localhost address.
window.location.hostname === '[::1]' || // 127.0.0.0/8 are considered localhost for IPv4.
window.location.hostname.match(/^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/));
function register(config) {
  if (false) {}
}

function registerValidSW(swUrl, config) {
  navigator.serviceWorker.register(swUrl).then(registration => {
    registration.onupdatefound = () => {
      const installingWorker = registration.installing;

      if (installingWorker == null) {
        return;
      }

      installingWorker.onstatechange = () => {
        if (installingWorker.state === 'installed') {
          if (navigator.serviceWorker.controller) {
            // At this point, the updated precached content has been fetched,
            // but the previous service worker will still serve the older
            // content until all client tabs are closed.
            console.log('New content is available and will be used when all ' + 'tabs for this page are closed. See https://bit.ly/CRA-PWA.'); // Execute callback

            if (config && config.onUpdate) {
              config.onUpdate(registration);
            }
          } else {
            // At this point, everything has been precached.
            // It's the perfect time to display a
            // "Content is cached for offline use." message.
            console.log('Content is cached for offline use.'); // Execute callback

            if (config && config.onSuccess) {
              config.onSuccess(registration);
            }
          }
        }
      };
    };
  }).catch(error => {
    console.error('Error during service worker registration:', error);
  });
}

function checkValidServiceWorker(swUrl, config) {
  // Check if the service worker can be found. If it can't reload the page.
  fetch(swUrl, {
    headers: {
      'Service-Worker': 'script'
    }
  }).then(response => {
    // Ensure service worker exists, and that we really are getting a JS file.
    const contentType = response.headers.get('content-type');

    if (response.status === 404 || contentType != null && contentType.indexOf('javascript') === -1) {
      // No service worker found. Probably a different app. Reload the page.
      navigator.serviceWorker.ready.then(registration => {
        registration.unregister().then(() => {
          window.location.reload();
        });
      });
    } else {
      // Service worker found. Proceed as normal.
      registerValidSW(swUrl, config);
    }
  }).catch(() => {
    console.log('No internet connection found. App is running in offline mode.');
  });
}

function unregister() {
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    }).catch(error => {
      console.error(error.message);
    });
  }
}

/***/ }),

/***/ 1:
/*!**************************************************************************************************************!*\
  !*** multi (webpack)/hot/dev-server.js ./node_modules/react-dev-utils/webpackHotDevClient.js ./src/index.js ***!
  \**************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Users/vladkalinichenko/Documents/Diploma/diploma/node_modules/webpack/hot/dev-server.js */"./node_modules/webpack/hot/dev-server.js");
__webpack_require__(/*! /Users/vladkalinichenko/Documents/Diploma/diploma/node_modules/react-dev-utils/webpackHotDevClient.js */"./node_modules/react-dev-utils/webpackHotDevClient.js");
module.exports = __webpack_require__(/*! /Users/vladkalinichenko/Documents/Diploma/diploma/src/index.js */"./src/index.js");


/***/ })

},[[1,"runtime-main",1]]]);
//# sourceMappingURL=main.chunk.js.map