webpackHotUpdate("main",{

/***/ "./node_modules/css-loader/dist/cjs.js?!./node_modules/postcss-loader/src/index.js?!./src/components/common/Menu/Menu.module.css":
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??ref--6-oneOf-4-1!./node_modules/postcss-loader/src??postcss!./src/components/common/Menu/Menu.module.css ***!
  \*******************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
var ___CSS_LOADER_GET_URL_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
var ___CSS_LOADER_URL_IMPORT_0___ = __webpack_require__(/*! ../../../assets/img/logo_arrow.svg */ "./src/assets/img/logo_arrow.svg");
exports = ___CSS_LOADER_API_IMPORT___(false);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = ___CSS_LOADER_GET_URL_IMPORT___(___CSS_LOADER_URL_IMPORT_0___);
// Module
exports.push([module.i, "\n.Menu_menu__27czC {\n    height: 100vh;\n    background: rgba(0, 0, 0, 0.4);\n    position: fixed;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    flex-direction: column;\n    transition: .5s;\n    left: 0;\n    top: 0;\n    z-index: 10;\n}\n\n.Menu_closedMenu__3Q2sd {\n    width: 80px;\n}\n\n.Menu_openedMenu__1k9Li {\n    width: 330px;\n}\n\n.Menu_logo___Mlk5 {\n    position: absolute;\n    left: 20%;\n    top: 50px;\n}\n\n.Menu_closedMenu__3Q2sd .Menu_logo___Mlk5 {\n    width: 60%;\n}\n\n.Menu_openedMenu__1k9Li .Menu_logo___Mlk5 {\n    width: 40%;\n}\n\n.Menu_menuButton__2vXjV {\n    transform: rotate(90deg);\n    background: 0;\n    color: #707070;\n    font-family: 'AribauBold';\n    font-size: 30px;\n    outline: none;\n    cursor: pointer;\n    transition: .3s;\n    border: 0;\n}\n\n.Menu_menuButton__2vXjV:hover {\n    color: #adacac;\n}\n\n.Menu_openedLogo__VtHhO {\n    position: absolute;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    left: 20%;\n    top: 50px;\n}\n\n.Menu_logo___Mlk5 .Menu_logoImg__eRjtK {\n    transition: .3s;\n    flex-shrink: 0;\n    width: 100px;\n    display: flex;\n    justify-content: center;\n}\n\n.Menu_logo___Mlk5::before {\n    position: absolute;\n    content: '';\n    width: 25px;\n    height: 25px;\n    top: 10px;\n    left: -30px;\n    background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat;\n    background-size: contain;\n    transform: rotate(180deg);\n}\n\n.Menu_logo___Mlk5::after {\n    position: absolute;\n    content: '';\n    width: 25px;\n    height: 25px;\n    top: 10px;\n    right: -30px;\n    background: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") no-repeat;\n    background-size: contain;\n}\n\n.Menu_logoImg__eRjtK img {\n    width: 100%;\n}\n\n.Menu_closedLogo__3l5xj .Menu_logoImg__eRjtK {\n    width: 0px;\n    overflow: hidden;\n}", ""]);
// Exports
exports.locals = {
	"menu": "Menu_menu__27czC",
	"closedMenu": "Menu_closedMenu__3Q2sd",
	"openedMenu": "Menu_openedMenu__1k9Li",
	"logo": "Menu_logo___Mlk5",
	"menuButton": "Menu_menuButton__2vXjV",
	"openedLogo": "Menu_openedLogo__VtHhO",
	"logoImg": "Menu_logoImg__eRjtK",
	"closedLogo": "Menu_closedLogo__3l5xj"
};
module.exports = exports;


/***/ })

})
//# sourceMappingURL=main.b32b32d245238c60350b.hot-update.js.map