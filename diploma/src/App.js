import React, { Suspense, useState, useEffect } from 'react';
import { Route, withRouter, useLocation, Redirect } from "react-router-dom";
import Loader from './components/common/Loader/Loader';
import AdgendaContainer from './components/Adgenda/AdgendaContainer';
import Login from './components/common/Login/Login';
import './App.css';
import AdminPanelContainer from './components/AdminPanel/AdminPanelContainer';
import MoreInfo from './components/Meetup/MoreInfo/MoreInfo';
const MeetupContainer = React.lazy(() => import('./components/Meetup/MeetupContainer'));

function App(props) {

  return (
    <div className="app-wrapper">
      <Route exact path='/' render={() => <Redirect to="/meetup/" />} />
      <Route path='/meetup/:id?'
        render={() => <Suspense fallback={<Loader/>}><MeetupContainer /></Suspense>} />

      <Route path='/adgenda/:id'
        render={() => <AdgendaContainer />} />

      <Route path='/admin'
        render={() => <AdminPanelContainer />} />

    </div>
  );
}

export default  (withRouter)(App);
