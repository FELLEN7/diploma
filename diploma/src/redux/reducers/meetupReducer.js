const CREATE_MEETUP = 'CREATE_MEETUP';
const GET_ALL_MEETUPS = 'GET_ALL_MEETUPS';
const DELETE_MEETUP = 'DELETE_MEETUP';

let initialState = {
    meetups: [],
    selectedMeetup: {
        name: '',
        date: '',
        venue: '',
        guests: [],
        leaders: [],
        ambassador: {},
        adgenda: [],
        mission: '',
        afterparty: [],
        food: '',
        drinks: '',
        invitation: ''
    }
}

const meetupReducer = (state = initialState, action) => {
    switch (action.type) {
        default:
            return state;
    }
}

export default meetupReducer;
