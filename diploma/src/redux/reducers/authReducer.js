let inititalState = {
  userId: null,
  email: null,
  login: null,
  isAuth: false
}

const authReducer = (state = inititalState, action) => {
  switch (action.type) {
    default:
      return state;
  }
}

export default authReducer;