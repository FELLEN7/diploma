import {createStore, combineReducers, applyMiddleware} from 'redux';
import authReducer from './reducers/authReducer';
import meetupReducer from './reducers/meetupReducer';
import thunkMiddleware from 'redux-thunk';


let reducers = combineReducers({
  auth: authReducer,
  meetup: meetupReducer
})

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;