import React, { useState } from 'react';
import twitter from '../../../assets/img/logo-twitter.svg';
import facebook from '../../../assets/img/logo-facebook.svg';
import google from '../../../assets/img/logo-google.svg';
import s from './Login.module.css';
import { Link } from 'react-router-dom';
import * as axios from 'axios';
import { Config } from '../../../config';

const Login = (props) => {

    const [isAuth, setAuth] = useState(props.isAuth);
    const [isSignIn, setSignIn] = useState(false);
    const [isPromotions, setPromotions] = useState(false);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = async (e) => {
        e.preventDefault();

        let response = await axios.post(`${Config.apiUrl}login`,
            { email, password })
            .then(response => {
                localStorage.setItem('token', response.data.token);
                setAuth(false);
            })
            .catch(err => {
                console.log(err);
            })

        console.log(response);
    }


    return (
        <>
            {
                isAuth && (
                    <div className={s.loginContainer}>
                        <div className={s.login}>
                            <div className={s.selector}>
                                <button className={!isSignIn ? s.active : ''} onClick={() => { setSignIn(false) }}>Sign up</button>
                                <button className={isSignIn ? s.active : ''} onClick={() => { setSignIn(true) }}>Sign in</button>
                            </div>
                            <span>With you social network</span>
                            <div className={s.signUp}>
                                <div className={s.socials}>
                                    <Link to="#">
                                        <img src={facebook} alt="facebook" />
                                    </Link>
                                    <Link to="#">
                                        <img src={twitter} alt="twitter" />
                                    </Link>
                                    <Link to="#">
                                        <img src={google} alt="google" />
                                    </Link>
                                </div>
                            </div>
                            <span className={s.lines}>or</span>
                            {
                                isSignIn
                                    ? <form>
                                        <div className={s.inputBox}>
                                            <input type="text" placeholder="Username" />
                                        </div>
                                        <div className={s.inputBox}>
                                            <input type="email" placeholder="Email" />
                                        </div>
                                        <div className={s.inputBox}>
                                            <input type="password" placeholder="Password" />
                                        </div>
                                        <button className={s.registerBtn}>Register to be a member</button>
                                        <div className={s.agreementBox}>
                                            <div
                                                onClick={() => setPromotions(!isPromotions)}
                                                className={s.checkBox}>
                                                <div className={isPromotions ? s.checkBoxActive : ''}></div>
                                            </div>
                                            <span>I do not wish to receive news and promotions from {'<Geekle>'} company by email.</span>
                                        </div>
                                        <p>Already have an account? <span>Sign in</span></p>
                                    </form>
                                    : <form>
                                        <div className={s.inputBox}>
                                            <input
                                                type="email"
                                                onChange={(e) => {setEmail(e.target.value)}}
                                                value={email}
                                                placeholder="Username or Email"
                                            />
                                        </div>
                                        <div className={s.inputBox}>
                                            <input
                                                type="password"
                                                onChange={(e) => {setPassword(e.target.value)}}
                                                value={password}
                                                placeholder="Password"
                                            />
                                        </div>
                                        <button onClick={(e) => login(e)} className={s.registerBtn}>Login</button>
                                        <div className={s.agreementBox}>
                                            <div
                                                onClick={() => setPromotions(!isPromotions)}
                                                className={s.checkBox}>
                                                <div className={isPromotions ? s.checkBoxActive : ''}></div>
                                            </div>
                                            <span>Remember me</span>
                                        </div>
                                    </form>
                            }
                        </div>
                    </div>
                )
            }
        </>
    )
}

export default Login;
