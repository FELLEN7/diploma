import React, { useEffect, useState } from 'react';
import s from './PastEvents.module.css';
import reportImg1 from '../../../assets/img/report2.png';
import reportImg2 from '../../../assets/img/report1.png';

const PastEvents = (props) => {

    const [eventsIsOpen, setEvents] = useState(false);

    useEffect(() => {
    }, [eventsIsOpen]);

    const toggleMenu = () => {
        setEvents(!eventsIsOpen);
    }

    const menuStyle = `${eventsIsOpen ? s.openedEvents : s.closedEvents} ${s.pastEvents}`;

    return (
        <div className={menuStyle}>
            <div className={s.buttonBox}>
                <button onClick={toggleMenu} className={s.pastEventsButton}>Past Events</button>
            </div>
            <div className={s.eventsBox}>
                <h3>Past Events:</h3>
                <div className={s.eventBox}>
                    <time><span>21</span>th of <span>March</span></time>
                    <div className={s.members}>
                        <span>32</span>
                        <span>Members</span>
                    </div>
                    <h4>Javascript</h4>
                    <h5>Topic:</h5>
                    <ul className={s.topics}>
                        <li>Lorem ipsum dolor sit</li>
                        <li>Amet, consectetur </li>
                        <li>Edipiscing elit, sed do</li>
                    </ul>
                    <h5>Photo report:</h5>
                    <div className={s.photoReport}>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                        <div>
                            <img src={reportImg2} alt="" />
                        </div>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                    </div>
                    <button className={s.openBtn}>Open</button>
                </div>
                <div className={s.eventBox}>
                    <time><span>21</span>th of <span>March</span></time>
                    <div className={s.members}>
                        <span>32</span>
                        <span>Members</span>
                    </div>
                    <h4>Javascript</h4>
                    <h5>Topic:</h5>
                    <ul className={s.topics}>
                        <li>Lorem ipsum dolor sit</li>
                        <li>Amet, consectetur </li>
                        <li>Edipiscing elit, sed do</li>
                    </ul>
                    <h5>Photo report:</h5>
                    <div className={s.photoReport}>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                        <div>
                            <img src={reportImg2} alt="" />
                        </div>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                    </div>
                    <button className={s.openBtn}>Open</button>
                </div>
                <div className={s.eventBox}>
                    <time><span>21</span>th of <span>March</span></time>
                    <div className={s.members}>
                        <span>32</span>
                        <span>Members</span>
                    </div>
                    <h4>Javascript</h4>
                    <h5>Topic:</h5>
                    <ul className={s.topics}>
                        <li>Lorem ipsum dolor sit</li>
                        <li>Amet, consectetur </li>
                        <li>Edipiscing elit, sed do</li>
                    </ul>
                    <h5>Photo report:</h5>
                    <div className={s.photoReport}>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                        <div>
                            <img src={reportImg2} alt="" />
                        </div>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                    </div>
                    <button className={s.openBtn}>Open</button>
                </div>
            </div>
        </div>
    )
}

export default PastEvents;
