import React, { useEffect, useState } from 'react';
import s from './CitySelector.module.css';
import compass from '../../../../assets/img/compass.svg';

const CitySelector = (props) => {

    const [isSelectorOpen, setSelector] = useState(false);

    useEffect(() => {
    }, [isSelectorOpen]);

    const toggleSelector = () => {
        setSelector(!isSelectorOpen);
    }

    const selectorStyle = `${isSelectorOpen ? s.selectionOpened : s.selectionClosed} ${s.selectedCity}`

    return (
        <div className={s.citySelector}>
            <div onClick={() => toggleSelector()} className={selectorStyle}>
                <img src={compass} />
                <span>{props.selectedCity.name}</span>
            </div>
            {
                isSelectorOpen &&
                <ul className={s.customSelect}>
                    {
                        props.cities &&
                        props.cities.map(city => (
                            <li
                                className={s.option}
                                onClick={() => {
                                    props.selectCity(city.id)
                                    toggleSelector()
                                }}
                                key={city.id}
                            >
                                {city.name}
                            </li>
                        ))
                    }
                </ul>
            }
        </div>
    )
}

export default CitySelector;