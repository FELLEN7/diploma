import React, { useEffect } from 'react';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { withRouter } from 'react-router-dom';
import Menu from './Menu';

const MenuContainer = (props) => {

    const GET_MEETUPS = gql`
    {
        meetups {
        id
        name
        date
        }
    }`;

    const { loading, error, data } = useQuery(GET_MEETUPS);

    return (
        <Menu
            loading={loading}
            data={data}
            error={error}
            selectedCity={props.selectedCity}
            selectCity={props.selectCity}
            cities={props.cities}
            months={props.months}
        />
    )
}


export default MenuContainer;
