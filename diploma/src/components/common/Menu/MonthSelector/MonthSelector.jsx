
import React, { useState } from 'react';
import s from './MonthSelector.module.css';
import calendar from '../../../../assets/img/calendar.svg';

const MonthSelector = (props) => {

    const [selectedMonth, setMonth] = useState(8);
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    const nextMonth = (toNext) => {
        const prevMonth = selectedMonth <= 0 ? 0 : selectedMonth - 1;
        const nextMonth = selectedMonth >= 11 ? 11 : selectedMonth + 1;
        setMonth(toNext ? nextMonth : prevMonth);
    }

    return (
        <div className={s.monthSelector}>
            <button onClick={() => nextMonth(false)}></button>
            <div className={s.selectedMonth}>
                <img src={calendar} />
                <span>
                    {months[selectedMonth]}
                </span>
            </div>
            <button onClick={() => nextMonth(true)}></button>
        </div>
    )
}

export default MonthSelector;