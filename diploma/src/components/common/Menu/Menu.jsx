import React, { useEffect, useState } from 'react';
import s from './Menu.module.css';
import CitySelector from './CitySelector/CitySelector';
import geekle from '../../../assets/img/geekle.svg';
import MonthSelector from './MonthSelector/MonthSelector';
import miniTable from '../../../assets/img/mini_table.svg';
import { Link, useRouteMatch } from 'react-router-dom';

const Menu = (props) => {

    const [menuIsOpen, setMenu] = useState(false);
    const [meetups, setMeetups] = useState([]);

    useEffect(() => {
        if (props.data) {
            setMeetups(props.data.meetups);
        }
    }, [menuIsOpen, props.data]);

    const toggleMenu = () => {
        setMenu(!menuIsOpen);
    }

    const menuStyle = `${menuIsOpen ? s.openedMenu : s.closedMenu} ${s.menu}`;
    const logoStyle = `${menuIsOpen ? null : s.closedLogo} ${s.logo}`;
    const match = useRouteMatch();

    return (
        <div className={menuStyle}>
            <div className={logoStyle}>
                <Link to={'/'}>
                    <div className={s.logoImg}>
                        <img src={geekle} alt='geekle' />
                    </div>
                </Link>
            </div>
            <div className={s.filterBox}>
                <h2>Upcoming Events</h2>
                <CitySelector
                    selectedCity={props.selectedCity}
                    selectCity={props.selectCity}
                    cities={props.cities}
                />
                <MonthSelector />
                <div className={s.eventSelector}>
                    {
                        props.data &&
                        meetups.map(meetup => {
                            let {name, date, id} = meetup;
                            date = date.split('-');
                            return (
                                <Link key={id} to={`./${id}`} onClick={() => toggleMenu()}>
                                    <div className={s.event}>
                                        <img src={miniTable} />
                                        <div className={s.eventDescription}>
                                            <h4>{name}</h4>
                                            <time>{date[2]}<span>th of </span>{props.months[Number(date[1])]}</time>
                                        </div>
                                    </div>
                                </Link>
                            )
                        })
                    }
                </div>
            </div>
            <div className={s.buttonBox}>
                <button onClick={() => toggleMenu()} className={s.menuButton}>Menu</button>
            </div>
        </div>
    )
}

export default Menu;
