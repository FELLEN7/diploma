import React from 'react';
import s from './Loader.module.css';
import loader from '../../../assets/img/loader.svg';

const Loader = (props) => {

    return (
        <div className={s.loader}>
            <img src={loader} alt=""/>
        </div>
    )
}

export default Loader;
