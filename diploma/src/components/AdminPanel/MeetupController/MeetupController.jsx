import React, { useState, useEffect } from 'react';
import { Route, useRouteMatch } from 'react-router-dom';
import MeetupCreatorContainer from './MeetupCreator/MeetupCreatorContainer';
import AllMeetupsContainer from './AllMeetups/AllMeetupsContainer';
import s from './MeetupController.module.css';
import { NavLink } from "react-router-dom";

const MeetupController = (props) => {

    const match = useRouteMatch();

    return (
        <div>
            <ul className="nav nav-tabs" style={{color: "black", marginTop: "40px"}}>
                <li className={s.dark} className="nav-item">
                    <NavLink to={`${match.path}/create-meetup`} className="nav-link">Create</NavLink>
                </li>
                <li className={s.dark} className="nav-item">
                    <NavLink to={`${match.path}/all-meetups`} className="nav-link">Get All</NavLink>
                </li>
            </ul>
            <Route path={`${match.path}/create-meetup/:id?`}
                render={() => <MeetupCreatorContainer/>} />
            <Route path={`${match.path}/all-meetups`}
                render={() => <AllMeetupsContainer/>} />
        </div>
    )
}

export default MeetupController;
