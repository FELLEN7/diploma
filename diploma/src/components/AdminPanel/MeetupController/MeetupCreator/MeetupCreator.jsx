import React, { useState, useEffect } from 'react';
import s from './MeetupCreator.module.css';

const MeetupCreator = (props) => {

    const [name, setName] = useState({ value: '', error: false });
    const [venue, setVenue] = useState('');
    const [date, setDate] = useState('');
    const [mission, setMission] = useState('');
    const [food, setFood] = useState('');
    const [drinks, setDrinks] = useState('');

    const [adgendaTime, setAdgendaTime] = useState('');
    const [adgendaEvent, setAdgendaEvent] = useState('');
    const [adgenda, setAdgenda] = useState([]);

    const [leaderName, setLeaderName] = useState('');
    const [leaderTopic, setLeaderTopic] = useState('');
    const [leaders, setLeaders] = useState([]);

    const [ambassadorName, setAmbassadorName] = useState('');
    const [ambassadors, setAmbassadors] = useState([]);

    useEffect(() => {
        if (props.data) {
            const meetup = props.data.meetup;
            setName({ value: meetup.name, error: false });
            setVenue(meetup.venue);
            setDate(meetup.date);
            setMission(meetup.mission);
            setFood(meetup.food);
            setDrinks(meetup.drinks);
            setAdgenda(meetup.adgenda.map((e, i) => ({ ...e, id: Date.now() * i })));
            setLeaders(meetup.leaders.map((e, i) => ({ ...e, id: Date.now() * i })));
            setAmbassadors(meetup.ambassadors.map((e, i) => ({ name: e, id: Date.now() * i })));
        }
        if(props.updatingData) {
            props.refetch();
        }
    }, [props.data, props.updatingData])

    const adgendaIsValid = () => {
        return (adgendaTime.trim() !== '') && (adgendaEvent.trim() !== '');
    }

    const addAdgenda = (e) => {
        e.preventDefault();
        if (adgendaIsValid()) {
            setAdgenda([...adgenda, { id: Date.now(), time: adgendaTime, event: adgendaEvent }]);
        }
    }

    const addLeader = (e) => {
        e.preventDefault();
        if (basicValidate(leaderName)) {
            setLeaders([...leaders, { id: Date.now(), name: leaderName, topic: leaderTopic }]);
        }
    }

    const addAmbassador = (e) => {
        e.preventDefault();
        if (basicValidate(ambassadorName)) {
            setAmbassadors([...ambassadors, { id: Date.now(), name: ambassadorName }])
        }
    }

    const submit = (e) => {
        e.preventDefault();
        alert('saved');
        props.createMeetup({
            variables: {
                name: name.value,
                date,
                venue,
                food,
                drinks,
                ambassadors: ambassadors.map(ambassador => ambassador.name),
                mission,
                adgenda: adgenda.map(adgenda => ({ time: adgenda.time, event: adgenda.event })),
                leaders: leaders.map(leader => ({ name: leader.name, topic: leader.topic, photo: 'https://futurefaces.com/media/image/resized/c_ca6e3a54365669dc_600x800.jpg' }))
            }
        })
    }

    const update = (e) => {
        e.preventDefault();
        props.updateMeetup({
            variables: {
                id: props.data.meetup.id,
                name: name.value,
                date,
                venue,
                food,
                drinks,
                ambassadors: ambassadors.map(ambassador => ambassador.name),
                mission,
                adgenda: adgenda.map(adgenda => ({ time: adgenda.time, event: adgenda.event })),
                leaders: leaders.map(leader => ({ name: leader.name, topic: leader.topic, photo: 'https://futurefaces.com/media/image/resized/c_ca6e3a54365669dc_600x800.jpg' }))
            }
        })
    }

    const basicValidate = (value) => {
        return value.trim() !== '';
    }

    const deleteAdgenda = (e, id) => {
        e.preventDefault();
        setAdgenda([...adgenda.filter(e => e.id !== id)]);
    }

    const deleteLeader = (e, id) => {
        e.preventDefault();
        setLeaders([...leaders.filter(e => e.id !== id)]);
    }

    const deleteAmbassador = (e, id) => {
        e.preventDefault();
        setAmbassadors([...ambassadors.filter(e => e.id !== id)]);
    }

    return (
        <form className={s.margin + ' ' + 'container'}>
            {/* <div className={s.alert + ' ' + 'alert alert-warning'} role="alert">
                error
            </div> */}
            <div className="row">
                <div className="col-sm form-group">
                    <div className="form-group">
                        <label htmlFor="name">Meetup Name</label>
                        <input
                            type="text"
                            className={'form-control' + ` ${name.error ? s.errorBorder : ' '}`}
                            value={name.value}
                            onChange={(e) => setName({ value: e.target.value, error: basicValidate(e.target.value) ? '' : true })}
                        />
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label htmlFor="venue">Venue</label>
                            <select
                                value={venue}
                                onChange={(e) => { setVenue(e.target.value) }}
                                className="custom-select"
                                id="venue"
                            >
                                <option defaultValue disabled>Choose...</option>
                                <option>Kiev, ZVD</option>
                                <option>Kiev</option>
                                <option>Kiev</option>
                            </select>
                        </div>
                        <div className='col-sm-6'>
                            <div className="form-group">
                                <label htmlFor="inputDate">Date</label>
                                <input
                                    value={date}
                                    onChange={(e) => setDate(e.target.value)}
                                    type="date"
                                    className="form-control"
                                />
                            </div>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="mission">Mission</label>
                        <textarea
                            value={mission}
                            onChange={(e) => setMission(e.target.value)}
                            className="form-control"
                            id="mission"
                            rows="3">
                        </textarea>
                    </div>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <label htmlFor="food">Food</label>
                            <textarea
                                value={food}
                                onChange={(e) => setFood(e.target.value)}
                                className="form-control"
                                id="food" rows="3">
                            </textarea>
                        </div>
                        <div className="form-group col-md-6">
                            <label htmlFor="drinks">Drinks</label>
                            <textarea
                                value={drinks}
                                onChange={e => setDrinks(e.target.value)}
                                className="form-control"
                                id="drinks"
                                rows="3">
                            </textarea>
                        </div>
                    </div>
                    <div className={s.adgenda}>
                        <div className="form-row">
                            <div className="input-group form-group col-sm-4">
                                <input
                                    value={adgendaTime}
                                    onChange={e => setAdgendaTime(e.target.value)}
                                    type="time"
                                    className="form-control"
                                    id="time"
                                    placeholder="Time" />
                            </div>
                            <div className="input-group form-group col-sm-5">
                                <input
                                    value={adgendaEvent}
                                    onChange={e => setAdgendaEvent(e.target.value)}
                                    type="text"
                                    className="form-control"
                                    id="event"
                                    placeholder="Event"
                                />
                            </div>
                            <div className="input-group form-group col-sm-2">
                                <button onClick={(e) => addAdgenda(e)} className="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                        <div className={s.adgendaTable + ' .table-responsive-sm'}>
                            <table className="table">
                                <tbody>
                                    {
                                        (adgenda && adgenda.length !== 0) ?
                                            adgenda.map(adgenda => (
                                                <tr className={s.center} key={adgenda.id}>
                                                    <td>{adgenda.time}</td>
                                                    <td>{adgenda.event}</td>
                                                    <td><button
                                                        onClick={e => deleteAdgenda(e, adgenda.id)}
                                                        className={s.deletBtn + ' bg-danger'}
                                                    >
                                                        Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            ))
                                            :
                                            <tr className={s.center}>
                                                <td>empty</td>
                                            </tr>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="col-sm">
                    <div className={s.adgenda} style={{ marginBottom: "20px" }}>
                        <label htmlFor="mission">Leaders</label>
                        <div className="form-row">
                            <div className="input-group form-group col-sm-4">
                                <input
                                    value={leaderName}
                                    onChange={e => setLeaderName(e.target.value)}
                                    type="text"
                                    className="form-control"
                                    placeholder="Leader Name"
                                />
                            </div>
                            <div className="input-group form-group col-sm-5">
                                <input
                                    value={leaderTopic}
                                    onChange={e => setLeaderTopic(e.target.value)}
                                    type="text"
                                    className="form-control"
                                    placeholder="Topic"
                                />
                            </div>
                            <div className="input-group form-group col-sm-2">
                                <button
                                    onClick={(e) => addLeader(e)}
                                    className="btn btn-primary"
                                >
                                    Add
                                </button>
                            </div>
                        </div>
                        <div className={s.adgendaTable + ' .table-responsive-sm'}>
                            <table className="table">
                                <tbody>
                                    {
                                        (leaders && leaders.length !== 0) ?
                                            leaders.map(leader => (
                                                <tr className={s.center} key={leader.id}>
                                                    <td>{leader.name}</td>
                                                    <td>{leader.topic}</td>
                                                    <td><button
                                                        onClick={e => deleteLeader(e, leader.id)}
                                                        className={s.deletBtn + ' ' + "bg-danger"}
                                                    >
                                                        Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            ))
                                            :
                                            <tr className={s.center}>
                                                <td>empty</td>
                                            </tr>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className={s.adgenda + ' ' + "form-group"}>
                        <label htmlFor="mission">Ambassadors</label>
                        <div className="form-row">
                            <div className="col-9">
                                <input
                                    value={ambassadorName}
                                    onChange={e => setAmbassadorName(e.target.value)}
                                    type="text"
                                    className="form-control"
                                    placeholder="Name Surname"
                                />
                            </div>
                            <div className="col-3">
                                <button
                                    onClick={e => addAmbassador(e)}
                                    className="btn btn-primary"
                                >
                                    Add
                                </button>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className={s.adgendaTable}>
                                <table className="table">
                                    <tbody>
                                        {
                                            ambassadors && ambassadors.length !== 0 &&
                                            ambassadors.map(ambassador => (
                                                <tr key={ambassador.id}>
                                                    <td className="text-black-50">{ambassador.name}</td>
                                                    <td>
                                                        <button
                                                            onClick={e => deleteAmbassador(e, ambassador.id)}
                                                            className={s.deletBtn + ' ' + "bg-danger"}
                                                        >
                                                            Delete
                                                        </button>
                                                    </td>
                                                </tr>
                                            ))
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {
                        props.data
                            ? <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={update}
                            >
                                Update Meetup
                            </button>
                            : <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={submit}
                            >
                                Create Meetup
                            </button>
                    }
                </div>
            </div>
        </form>
    )
}

export default MeetupCreator;
