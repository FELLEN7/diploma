import React, { useState, useEffect } from 'react';
import MeetupCreator from './MeetupCreator';
import gql from 'graphql-tag';
import { useMutation, useQuery } from '@apollo/react-hooks';
import { withRouter } from 'react-router-dom';

const MeetupCreatorContainer = (props) => {

  const [meetupId, setMeetupId] = useState('');

  useEffect(() => {
    props.match.params.id
      ? setMeetupId(props.match.params.id)
      : setMeetupId('');
  });

  const ADD_MEETUP = gql`
  mutation addMeetup(
      $name: String!,
      $date: String!,
      $venue: String!,
      $food: String!,
      $drinks: String!,
      $adgenda: [AdgendaInput],
      $leaders: [LeaderInput],
      $ambassadors: [String],
      $mission: String
      ) {
      addMeetup(
          name: $name,
          date: $date,
          venue: $venue,
          food: $food,
          drinks: $drinks,
          adgenda:$adgenda,
          leaders: $leaders,
          ambassadors: $ambassadors,
          mission: $mission
          ) {
        name,
        date,
        venue,
        food,
        drinks,
        ambassadors,
        mission,
        adgenda {
          time,
          event
        },
        leaders {
          name
          topic
          photo
        }
      }
    }
  `;

  const GET_MEETUP_BY_ID = gql`
query meetup($_id: ID) {
  meetup(_id: $_id) {
        id
        name
        mission
        date
        food
        drinks
        venue
        adgenda {
          time
          event
        }
        leaders {
            id
            name
            topic
            photo
        }
        ambassadors
    }
}
`;

const UPDATE_MEETUP = gql `
mutation updateMeetup(
  $id: ID!,
  $name: String!,
  $date: String!,
  $venue: String!,
  $food: String!,
  $drinks: String!,
  $adgenda: [AdgendaInput],
  $leaders: [LeaderInput],
  $ambassadors: [String],
  $mission: String
) {
  updateMeetup(
    id: $id,
    name: $name,
    date: $date,
  	venue: $venue,
  	food: $food,
  	drinks: $drinks,
  	adgenda: $adgenda,
  	leaders: $leaders,
  	ambassadors: $ambassadors,
  	mission: $mission) {
    id,
    name,
    date,
    venue,
  	food,
  	drinks,
  	adgenda {
      time
      event
    },
  	leaders {
    	name,
      topic,
      photo
    },
  	ambassadors,
  	mission
  }
}`

  const { loading, error, data, refetch } = useQuery(GET_MEETUP_BY_ID, { variables: { _id: meetupId } });

  const [createMeetup] = useMutation(ADD_MEETUP);

  const [updateMeetup, updatingData] = useMutation(UPDATE_MEETUP);

  return (
    <>
      <MeetupCreator
        loading={loading}
        error={error}
        refetch={refetch}
        data={meetupId ? data : null}
        createMeetup={createMeetup}
        updateMeetup={updateMeetup}
        updatingData={updatingData}
      />
    </>
  )
}


export default (withRouter)(MeetupCreatorContainer);
