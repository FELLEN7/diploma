import React, { useEffect } from 'react';
import AllMeetups from './AllMeetups';
import gql from 'graphql-tag';
import { useQuery, useMutation } from '@apollo/react-hooks';

const AllMeetupsContainer = (props) => {

  const GET_MEETUPS = gql`
  {
    meetups {
      id
      name
      date
      venue
      adgenda {
        time,
        event
      }
      ambassadors
    }
  }`;

  const DELETE_MEETUP = gql`
    mutation deleteMeetup($id: ID) {
      deleteMeetup(id: $id) {
        id
      }
  }`;

  const [deleteMeetup, deletedData] = useMutation(DELETE_MEETUP);
  const { loading, error, data, refetch } = useQuery(GET_MEETUPS);

  return (
    <AllMeetups
      loading={loading}
      error={error}
      data={data}
      refetch={refetch}
      deleteMeetup={deleteMeetup}
      deletedData={deletedData}
    />
  )
}


export default AllMeetupsContainer;
