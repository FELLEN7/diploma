import React, { useState, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';
import s from './AllMeetups.module.css';

const AllMeetups = (props) => {

    const match = useRouteMatch();
    const { loading, error, data, refetch } = props;

    useEffect(() => {
        if(props.deletedData && !props.deletedData.error) {
            refetch();
        }
    })

    if (loading) return 'Loading...';
    if (error) return `Error! ${error.message}`;

    const deleteMeetup = (id) => props.deleteMeetup({ variables: { id } });

    return (
        <Container className={s.adminMeetup} fluid>
            <Table bordered hover>
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Meetup ID</th>
                        <th>Meetup Name</th>
                        <th>Date</th>
                        <th>Venue</th>
                        <th>Ambassador</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        !loading && !error &&
                        data.meetups.map((meetup, i) => (
                            <tr key={meetup.id}>
                                <td>{i + 1}</td>
                                <td>{meetup.id}</td>
                                <td>{meetup.name}</td>
                                <td>{meetup.date}</td>
                                <td>{meetup.venue}</td>
                                <td>{meetup.ambassadors[0]}</td>
                                <td><Link to={`../meetup/create-meetup/${meetup.id}`}><button className="btn btn-primary">update</button></Link></td>
                                <td><button className="btn btn-danger" onClick={() => deleteMeetup(meetup.id)}>delete</button></td>
                            </tr>
                        ))
                    }
                </tbody>
            </Table>
        </Container>
    )
}

export default AllMeetups;
