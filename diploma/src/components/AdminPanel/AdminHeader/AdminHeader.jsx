import React, { useState } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import AdminLogin from '../AdminLogin/AdminLogin';
import {
    BrowserRouter as Router,
    Link,
  } from "react-router-dom";
import s from './AdminHeader.module.css';

const AdminHeader = (props) => {

    const [isAuth, setAuth] = useState(false);

    const authorize = () => {
        setAuth(!isAuth);
    }

    return (
        <Container fluid className={s.removePadding}>
            <Navbar bg="dark" variant="dark">
                <Link to={'/'}><Navbar.Brand>Geekle</Navbar.Brand></Link>
                <Nav className="mr-auto">
                    <Link className="nav-link" exact="true" to={`/admin/meetup`}><Nav>Meetup</Nav></Link>
                </Nav>
                <button className="btn btn-primary my-2 my-sm-0" onClick={() => authorize()}>Login</button>
            </Navbar>
            {
                isAuth &&
                <AdminLogin/>
            }
        </Container>
    )
}

export default AdminHeader;
