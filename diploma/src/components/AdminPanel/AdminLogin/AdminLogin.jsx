import React, { useState } from 'react';
import s from './AdminLogin.module.css';

const AdminLogin = (props) => {

    const [isSignIn, setSignIn] = useState(false);

    return (
        <div className={s.loginContainer}>
            <form className={s.loginBox}>
                <input type="text" placeholder="Username" />
                <input type="password" placeholder="Password" />
                <button className="btn btn-primary">Login</button>
            </form>
        </div>
    )
}

export default AdminLogin;
