import React, { useState, useEffect } from 'react';
import AdminHeader from './AdminHeader/AdminHeader';
import { Route, useRouteMatch } from 'react-router-dom';
import MeetupControllerContainer from './MeetupController/MeetupControllerContainer';
import AdminLogin from './AdminLogin/AdminLogin';
import s from './AdminPanel.module.css';

const AdminPanel = (props) => {

    const match = useRouteMatch();
    
    return (
        <div className={s.adminContainer}>
            <AdminHeader/>
            <Route path={`${match.path}/meetup`}
                render={() => <MeetupControllerContainer />} />
        </div>
    )
}

export default AdminPanel;
