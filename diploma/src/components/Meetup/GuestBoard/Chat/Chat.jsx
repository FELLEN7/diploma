import React from 'react';
import s from './Chat.module.css';

const Chat = (props) => {

    return (
        <div className={s.chatContainer}>
            <div className={s.scrollBox}>
                <div className={s.messageBox}>
                    <div className={s.userImg}>
                        <img src='https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg' />
                    </div>
                    <div className={s.desription}>
                        <h4>Name Surname</h4>
                        <span className={s.smallFont + ' grey'}>Tech Speaker</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                    </div>
                </div>
                <div className={s.messageBox}>
                    <div className={s.userImg}>
                        <img src='https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg' />
                    </div>
                    <div className={s.desription}>
                        <h4>Name Surname</h4>
                        <span className={s.smallFont + ' grey'}>Tech Speaker</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                    </div>
                </div>
                <div className={s.messageBox}>
                    <div className={s.userImg}>
                        <img src='https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg' />
                    </div>
                    <div className={s.desription}>
                        <h4>Name Surname</h4>
                        <span className={s.smallFont + ' grey'}>Tech Speaker</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                    </div>
                </div>
                <div className={s.messageBox}>
                    <div className={s.userImg}>
                        <img src='https://i.pinimg.com/originals/9d/2b/df/9d2bdfe04f03354ffcbe0513ca9ba734.jpg' />
                    </div>
                    <div className={s.desription}>
                        <h4>Name Surname</h4>
                        <span className={s.smallFont + ' grey'}>Tech Speaker</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
                    </div>
                </div>
            </div>
            <div className={s.messageInput}>
                <input type='text' placeholder='message' />
                <button>
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.051" height="25.048" viewBox="0 0 25.051 25.048">
                        <path d="M88.3,64.1,64.347,74.535a.549.549,0,0,0,.02,1l6.478,3.66a1.045,1.045,0,0,0,1.194-.117L84.813,68.063c.085-.072.287-.209.365-.13s-.046.281-.117.365L74.009,80.746a1.041,1.041,0,0,0-.1,1.246l4.234,6.792a.551.551,0,0,0,.992-.013l9.9-23.95A.549.549,0,0,0,88.3,64.1Z" transform="translate(-64.036 -64.04)" fill="#00fcb6" />
                    </svg>
                </button>
            </div>
        </div>
    )
}

export default Chat;