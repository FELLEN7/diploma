import React, { useEffect, useState } from 'react';
import s from './GuestBoard.module.css';
import ChatContainer from './Chat/ChatContainer';

const GuestBoard = (props) => {

    const [members, setMembers] = useState([]);
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        if(props.members) {
            isOpen
                ? setMembers(props.members)
                : setMembers(props.members.filter((m, index) => index < 8));
        }
    }, [isOpen]);

    const getAllMembers = () => {
        setIsOpen(!isOpen);
    }

    return (
        <div className={s.guestBoard + ' ' + 'sideboard'}>
            <h4>Java Geekle Amsterdam </h4>
            <span className={s.bigFont}>245</span>
            <span className={s.smallFont}>members</span>
            <div className={s.guestList}>
                {
                    props.members &&
                    members.map((member) => (
                        <div key={member.id} className={s.memerImg}>
                            <img src={member.photo} />
                        </div>
                    ))
                }
            </div>
            <button onClick={() => getAllMembers()} className={s.seeAllBtn}>{isOpen ? 'Close' : 'See all'}</button>
            <h4>Chat</h4>
            <span className={s.bigFont}>9</span>
            <span className={s.smallFont}>Online</span>
            <ChatContainer />
        </div>
    )
}

export default GuestBoard;
