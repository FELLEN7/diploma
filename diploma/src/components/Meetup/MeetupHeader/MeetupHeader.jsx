import React, { useEffect, useState } from 'react';
import s from './MeetupHeader.module.css';

const MeetupHeader = (props) => {

    const [day, setDay] = useState('');
    const [month, setMonth] = useState('');
    const [name, setName] = useState('');
    const [mission, setMission] = useState('');

    useEffect(() => {
        if (props.data) {
            const meetup = props.data.meetup;
            const date = meetup.date.split('-');
            setDay(Number(date[2]));
            setMonth(props.months[Number(date[1]) - 1]);
            setName(meetup.name);
            setMission(meetup.mission);
        }
    }, [props.data, props.loading]);

    return (
        <header>
            {
                !props.loading &&
                <h2 className={s.title}>{name}</h2>
            }
            <p className={s.prompt}>{mission}</p>
            <time className={s.date}>
                <span>{!props.loading && day} </span>
                th of
                <span> {!props.loading && month}</span>
            </time>
        </header>
    )
}

export default MeetupHeader;
