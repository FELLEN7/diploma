import React, { useEffect, useState } from 'react';
import s from './LeaderBoard.module.css';
import * as d3 from 'd3';

const LeaderBoard = (props) => {

    const [selectedLeader, setLeader] = useState('');
    const [leaders, setLeaders] = useState([]);
    const [ambassador, setAmbassador] = useState('');

    useEffect(() => {
        if (props.data) {
            setLeaders(props.data.meetup.leaders);
            setLeader(props.data.meetup.leaders[0]);
            setAmbassador(props.data.meetup.ambassadors[0]);
        }
    }, [props.data, props.loading]);

    const selectLeader = (id) => {
        leaders.forEach(leader => leader.id === id ? setLeader(leader) : null)
    }

    return (
        <div className={s.leaderBoard + ' ' + 'sideboard'}>
            <div className={s.leaderSelector}>
                {/* {
                    props.loading &&
                    props.data.meetup.leaders.map(leader => {
                        let classes = `${selectedLeader._id === leader.id ? s.selected : null} ${s.leaderImg}`;
                        return (
                            <div key={leader._id}
                                 onClick={() => selectLeader(leader._id)}
                                 className={classes}>
                                <img src={leader.photo} />
                            </div>
                        )
                    })
                } */}
                {
                    !props.loading &&
                    leaders.map(leader => {
                        let classes = `${selectedLeader.id === leader.id ? s.selected : null} ${s.leaderImg}`;
                        return (
                            <div
                                key={leader.id}
                                onClick={() => selectLeader(leader.id)}
                                className={classes}>
                                <img src={leader.photo} />
                            </div>
                        )
                    })
                }
            </div>
            <span className='grey'>Choose a leader</span>
            <div className={s.infoBox + ' ' + s.underline}>
                {
                    props.leaders &&
                    <>
                        <h3>{selectedLeader.name}</h3>
                        <span className='grey'>{selectedLeader.type}</span>
                        <span>Topic:</span>
                        <p className={s.topic}>{selectedLeader.topic}</p>
                    </>
                }
            </div>
            <div className={s.infoBox + ' ' + s.ambassadorBox}>
                <h3>{!props.loading && ambassador}</h3>
                <span className='grey'>{props.ambassador.type}</span>
                <div className={s.ambassadorImgBox}>
                    <img className={s.ambassadorImg} src={props.ambassador.img} />
                </div>
            </div>
        </div>
    )
}

export default LeaderBoard;
