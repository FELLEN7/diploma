import React from 'react';
import LeaderBoard from './LeaderBoard';


const LeaderBoardContainer = (props) => {

    return (
      <LeaderBoard data={props.data} ambassador={props.ambassador}/>
    )
}


export default LeaderBoardContainer;
