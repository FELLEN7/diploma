import React, { useState, Suspense } from 'react';
import s from './Meetup.module.css';
import SpeakerImg from '../../assets/img/face.jpeg';
import Loader from '../common/Loader/Loader';
import { Route } from 'react-router-dom';
import MoreInfo from './MoreInfo/MoreInfo';
import Login from '../common/Login/Login';
const TableContainer = React.lazy(() => import('./Table/TableContainer'));
const MenuContainer = React.lazy(() => import('../common/Menu/Menu.container'));
const PastEvents = React.lazy(() => import('../common/PastEvents/PastEventsContainer'));
const LeaderBoardContainer = React.lazy(() => import('./LeaderBoard/LeaderBoardContainer'));
const GuestBoardContainer = React.lazy(() => import('./GuestBoard/GuestBoardContainer'));
const MeetupHeader = React.lazy(() => import('./MeetupHeader/MeetupHeader'));

const Meetup = (props) => {

    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    const [city, setCity] = useState(props.cities[0]);
    const [isAuthorize, setAuthorize] = useState(false);

    const selectCity = (id) => {
        props.cities.forEach(city => {
            if (city.id === id) {
                setCity(city);
            }
        })
    }

    return (
        <div className={s.meetupContainer}>
            <Suspense fallback={<Loader />}>
                <MenuContainer
                    selectedCity={city}
                    selectCity={selectCity}
                    cities={props.cities}
                    months={months}
                />
                <PastEvents />
                <Route exact path='/meetup/:id?'
                    render={() => <Suspense fallback={<Loader />}>
                        <GuestBoardContainer />
                        <LeaderBoardContainer loading={props.loading} data={props.data} ambassador={props.ambassador} />
                        <MeetupHeader loading={props.loading} data={props.data} months={months}/>
                        {
                            props.data &&
                            <TableContainer speakerImg={SpeakerImg} meetupId={props.data.meetup.id} loading={props.loading} refetch={props.refetch}/>
                        }
                    </Suspense>} />
                <Route path='/meetup/more-info/:id?'
                    render={() => <Suspense fallback={<Loader />}>
                        <MoreInfo />
                    </Suspense>} />
            </Suspense>
        </div>
    )
}

export default Meetup;
