import React, { useEffect, useState } from 'react';
import s from './Table.module.css';
import Seat from './Seat/Seat';
import { Link } from 'react-router-dom';
import Login from '../../common/Login/Login';
const testPhoto = 'https://i2-prod.mirror.co.uk/incoming/article14334083.ece/ALTERNATES/s615/3_Beautiful-girl-with-a-gentle-smile.jpg'

const Table = (props) => {

    const [isLogin, setLogin] = useState(false);
    const [tableSideSize, setTableSideSize] = useState(4);
    const [leftSideGuests, setLeftSideGuests] = useState([tableSideSize]);
    const [rightSideGuests, setRightSideGuests] = useState([tableSideSize]);

    useEffect(() => {
        if (props.data) {
            setLeftSideGuests(fillTableSide(0, tableSideSize));
            setRightSideGuests(fillTableSide(tableSideSize, tableSideSize * 2));
        }
    }, [props.data]);

    const fillTableSide = (start, end) => {
        let guests = [];
        for (let i = start; i < end; i++) {
            let guest = props.data.guestsByMeetupId[i]
                ? { ...props.data.guestsByMeetupId[i], isTaken: true }
                : { isTaken: false };
            guests.push(guest);
        }
        return guests;
    }

    return (
        <div className={s.tableContainer}>
            {/* <div className={s.speakerBox}>
                <span className={s.speakerType}>Tech Speaker:</span>
                <div className={s.speakerImg}>
                    <img src={props.speakerImg} />
                </div>
                <span>Name Surname</span>
            </div> */}
            <Login isAuth={isLogin}/>
            <div className={s.spaceBetween}>
                <ul className='seats'>
                    {
                        !props.loading &&
                        leftSideGuests.map(guest => (
                            <li key={Math.random()}>
                                <Seat
                                    photo={guest.photo}
                                    isLeft={true}
                                    isTaken={guest.isTaken}
                                    meetupId={props.meetupId}
                                    setLogin={setLogin}
                                    refetch={props.refetch}
                                    meetupLoading={props.meetupLoading}
                                />
                            </li>
                        ))
                    }
                </ul>
                <div className={s.table}>
                    <h4>Vlad Kalinichenko</h4>
                </div>
                <ul className='seats'>
                    {
                        !props.loading &&
                        rightSideGuests.map(guest => (
                            <li key={Math.random()}>
                                <Seat
                                    photo={guest.photo}
                                    isLeft={false}
                                    isTaken={guest.isTaken}
                                    meetupId={props.meetupId}
                                />
                            </li>
                        ))
                    }
                </ul>
            </div>
            <Link className={s.moreInfoBtn} to={`/meetup/more-info/${props.id}`} >
                MoreInfo
            </Link>
        </div>
    )
}

export default Table;
