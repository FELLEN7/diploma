import React, { useEffect } from 'react';
import Table from './Table';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { withRouter } from 'react-router-dom';

const TableContainer = (props) => {

    const GET_GUESTS = gql`
        query guestsByMeetupId($_id: ID) {
            guestsByMeetupId(_id: $_id) {
                name
                photo
            }
        }
    `;

    useEffect(() => {
        if(props.meetupId) {
            console.log(props.meetupId)
        }
    }, [props.meetupId, props.loading]);

    const { loading, error, data } = useQuery(GET_GUESTS, {
        variables: {
            _id: props.meetupId
        }
    });

    return (
        <Table
            loading={loading}
            data={data}
            error={error}
            meetupId={props.meetupId}
            refetch={props.refetch}
            meetupLoading={props.loading}
        />
    )
}


export default (withRouter)(TableContainer);
