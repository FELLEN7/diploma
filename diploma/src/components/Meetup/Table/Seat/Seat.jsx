import React, { useEffect, useRef, useState } from 'react';
import s from './Seat.module.css';
import defaultPhoto from '../../../../assets/img/default-photo.png';
import * as axios from 'axios';
import { Config } from '../../../../config';


const Seat = (props) => {
    const [meetupId, setMeetupId] = useState('');

    useEffect(() => {
        if (props.meetupId) {
            setMeetupId(props.meetupId);
        }
    }, [props.meetupId, props.refetch]);

    const boook = async (email, password, meetupId) => {
        let token = localStorage.getItem('token') || '';
        console.log(meetupId);
        if(token) {
            axios.post(`${Config.apiUrl}book`, { meetupId: meetupId }, { headers: { authorization: token } })
                .then(response => {
                    console.log(response.data);
                });
        } else {
            this.props.setLogin(true);
        }
    }

    const direction = props.isLeft ? s.left : s.right;
    const photoStyle = props.isLeft ? s.photoLeft : s.photoRight;

    return (
        <>
            {
                props.meetupId &&
                <div className={s.seat} onClick={() => boook(meetupId)}>
                    {
                        props.isTaken &&
                        <div className={s.photoBox + ' ' + photoStyle}>
                            {
                                props.photo
                                    ? <img src={props.photo} alt="" />
                                    : <img src={defaultPhoto} alt="" />
                            }
                        </div>
                    }
                    <div className={direction}>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="127.179"
                            height="142.461"
                            viewBox="0 0 127.179 142.461"
                        >
                            <g transform="translate(-731.157 -601.24)">
                                {
                                    props &&
                                    <path
                                        fill="#fff"
                                        d="M2110.329 817.917c-20.678 27.131-37.022 34.483-59.107 26.582-18.912-6.765-31.458-24.861-30.637-44.184.769-18.02 14.793-37.49 30.009-41.66 26.34-7.216 40.378-.849 60.068 28.582 11.153-7.6 13.56-15.823 4.831-25.33-7.149-7.786-15.238-14.717-23.045-21.885-14.25-13.083-49.378-11.714-62.474 2.34-26.018 27.931-28.749 81.853-5.7 112.617 12.962 17.308 49.3 24.6 64.035 12.371a339.529 339.529 0 0026.573-24.761c9.342-9.631 7.681-17.129-4.553-24.672z"
                                        opacity={props.isTaken ? "0.8" : "0.43"}
                                        transform="translate(-1262.527 -129.728)"></path>
                                }
                                <path
                                    fill="#fff"
                                    d="M2055.429 741.526c-23.176-.685-41.169 16.387-41.144 39.038a40.144 40.144 0 0080.283.376c.287-22.054-16.282-38.74-39.139-39.414z"
                                    opacity={props.isTaken ? "0.8" : "0.43"}
                                    transform="translate(-1250.957 -108.33)"
                                ></path>
                                <path
                                    fill="#fff"
                                    d="M2025.911 734.337c-21.021 19.606-23.446 44.6-21.839 70.2 1.258 20.106 5.885 39.505 23.748 53.376-21.415-41.099-20.12-82.013-1.909-123.576z"
                                    opacity={props.isTaken ? "0.8" : "0.43"}
                                    transform="translate(-1272.512 -122.888)"
                                ></path>
                                <g className='plus' transform="translate(785.794 654.119)">
                                    {
                                        !props.isTaken &&
                                        <path
                                            fill="#00fcb6"
                                            d="M183 164.889h-12.669v-12.668a2.721 2.721 0 10-5.442 0v12.669h-12.668a2.606 2.606 0 00-2.721 2.721 2.634 2.634 0 002.721 2.721h12.669V183a2.635 2.635 0 002.721 2.721 2.707 2.707 0 002.72-2.721v-12.669H183a2.721 2.721 0 100-5.442z"
                                            transform="translate(-149.5 -149.5)">
                                        </path>
                                    }
                                </g>
                            </g>
                        </svg>
                    </div>
                </div>
            }
        </>
    )
}

export default Seat;
