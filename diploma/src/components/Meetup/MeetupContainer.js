import React, { useEffect, useState } from 'react';
import Meetup from './Meetup';
import gql from 'graphql-tag';
import { useQuery } from '@apollo/react-hooks';
import { withRouter } from 'react-router-dom';

const MeetupContainer = (props) => {

    const [id, setId] = useState('');

    const GET_MEETUP = gql`
        query meetup($_id: ID) {
            meetup(_id: $_id) {
                id
                name
                date
                mission
                leaders {
                    id
                    name
                    topic
                    photo
                }
                ambassadors
            }
        }
  `;

  useEffect(() => {
    if (props.match.params.id) setId(props.match.params.id);
  }, [props]);

    const { loading, error, data, refetch } = useQuery(GET_MEETUP, { variables: { _id: id } });


    const cities = [
        {
            id: 'asfwrg23g3re',
            name: 'Kiev, Ukrain',
        },
        {
            id: '12r3wfg3g',
            name: 'Alicante, Spain',
        },
        {
            id: '12rfge54y23r',
            name: 'Gamburg, Country'
        }
    ]

    const ambassador = {
        id: '21rfdfwf423fsdasdfasdff2e',
        firstName: 'Mila',
        lastName: 'Surname',
        type: 'Ambassador',
        img: 'https://www.pngarts.com/files/3/Man-PNG-Free-Download.png'
    }

    return (
        <Meetup
            ambassador={ambassador}
            cities={cities}
            data={data}
            loading={loading}
            error={error}
            refetch={refetch}
            location={props.location}
        />
    )
}

export default (withRouter)(MeetupContainer);
