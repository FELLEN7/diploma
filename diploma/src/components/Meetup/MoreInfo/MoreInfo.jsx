import React, { useState, Suspense } from 'react';
import MeetupHeader from '../MeetupHeader/MeetupHeader';
import s from './MoreInfo.module.css';
import reportImg1 from '../../../assets/img/report1.png';
import reportImg2 from '../../../assets/img/report2.png';
import pizza from '../../../assets/img/pizza.svg';
// import Loader from '../../common/Loader/Loader';


const MoreInfo = (props) => {


    return (
        <div className={s.infoContainer}>
            <div className={s.sideContainer}>
                <div className={s.infoBox}>
                    <h3 className={s.adgendaTitle}>Adgenda: </h3>
                    <ul className={s.eventList}>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                    </ul>
                </div>
                <div className={s.infoBox}>
                    <h3 className={s.venueTitle}>Venue: </h3>
                    <div className={s.venuePhotos}>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                        <div>
                            <img src={reportImg2} alt="" />
                        </div>
                        <div>
                            <img src={reportImg1} alt="" />
                        </div>
                    </div>
                    <button className={s.locationBtn}>Show location</button>
                </div>
            </div>
            <div className={s.sideContainer}>
                <span className="grey">Last metup report:</span>
                <div className={s.videoBox}>
                </div>
                <div className={s.infoBox + ' ' + s.foodBox}>
                    <img src={pizza} alt='' />
                    <h3 className={s.foodTitle}>Food & Drinks: </h3>
                    <ul>
                        <li>Pizza, urgers</li>
                        <li>beer, coca-cola</li>
                    </ul>
                </div>
            </div>
            <div className={s.sideContainer}>
                <div className={s.infoBox}>
                    <h3 className={s.missionTitle}>Mission: </h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
                        Consectetur adipiscing elit, sed do eiusmod tempor incididunt
                    </p>
                </div>
                <div className={s.infoBox}>
                    <h3 className={s.afterpartyTitle}>Afterparty: </h3>
                    <ul className={s.eventList}>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                        <li>
                            <time>12:42</time>
                            <p>Dors open.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default MoreInfo;
