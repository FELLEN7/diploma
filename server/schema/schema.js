const graphql = require(`graphql`);

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLInputObjectType,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
 } = graphql;

const Meetup = require('../models/meetup');
const Guest = require('../models/guest');


const AdgendaType = new GraphQLObjectType({
  name: `Adgenda`,
  fields: () => ({
    time: { type: new GraphQLNonNull(GraphQLString) },
    event: { type: new GraphQLNonNull(GraphQLString) }
  }),
});

const adgendaInput = new GraphQLInputObjectType({
  name: "AdgendaInput",
  description: "Adgenda",
  fields: () => ({
    time: { type: new GraphQLNonNull(GraphQLString) },
    event: { type: new GraphQLNonNull(GraphQLString) }
  })
});

const LeaderType = new GraphQLObjectType({
  name: `Leader`,
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    topic: { type: new GraphQLNonNull(GraphQLString) },
    photo: { type: new GraphQLNonNull(GraphQLString) }
  }),
});

const leaderInput = new GraphQLInputObjectType({
  name: "LeaderInput",
  description: "Leader",
  fields: () => ({
    name: { type: new GraphQLNonNull(GraphQLString) },
    topic: { type: new GraphQLNonNull(GraphQLString) },
    photo: { type: new GraphQLNonNull(GraphQLString) }
  })
});


const MeetupType = new GraphQLObjectType({
  name: `Meetup`,
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    date: { type: new GraphQLNonNull(GraphQLString) },
    food: { type: new GraphQLNonNull(GraphQLString) },
    drinks: { type: new GraphQLNonNull(GraphQLString) },
    venue: { type: new GraphQLNonNull(GraphQLString) },
    adgenda: { type: new GraphQLList(AdgendaType) },
    leaders: { type: new GraphQLList(LeaderType) },
    ambassadors: { type: new GraphQLList(GraphQLString) },
    mission: { type: GraphQLString }
  })
});

const GuestType = new GraphQLObjectType({
  name: `Guest`,
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) },
    meetups: { type: new GraphQLList(GraphQLID)},
    photo: { type: GraphQLString }
  })
})

const Mutation = new GraphQLObjectType({
  name: `Mutation`,
  fields: {
    addGuest: {
      type: GuestType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        meetups: { type: new GraphQLList(GraphQLID) },
        photo: { type: GraphQLString }
      },
      resolve(parent, args) {
        const guest = new Guest({
          name: args.name,
          email: args.email,
          password: args.password,
          photo: args.photo
        });
        return guest.save();
      }
    },
    addMeetup: {
      type: MeetupType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        date: { type: new GraphQLNonNull(GraphQLString) },
        venue: { type: new GraphQLNonNull(GraphQLString) },
        food: { type: new GraphQLNonNull(GraphQLString) },
        drinks: { type: new GraphQLNonNull(GraphQLString) },
        adgenda: { type: new GraphQLList(adgendaInput) },
        leaders: { type: new GraphQLList(leaderInput) },
        ambassadors: { type: new GraphQLList(GraphQLString) },
        mission: { type: GraphQLString }
      },
      resolve(parent, args) {
        const meetup = new Meetup({
          name: args.name,
          date: args.date,
          venue: args.venue,
          food: args.food,
          drinks: args.drinks,
          ambassadors: args.ambassadors,
          adgenda: args.adgenda,
          leaders: args.leaders,
          mission: args.mission
        });
        return meetup.save();
      }
    },
    deleteMeetup: {
      type: MeetupType,
      args: {
        id: { type: GraphQLID }
      },
      resolve(parent, args) {
        return Meetup.findByIdAndDelete(args.id);
      }
    },
    updateMeetup: {
      type: MeetupType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        name: { type: new GraphQLNonNull(GraphQLString) },
        date: { type: new GraphQLNonNull(GraphQLString) },
        venue: { type: new GraphQLNonNull(GraphQLString) },
        food: { type: new GraphQLNonNull(GraphQLString) },
        drinks: { type: new GraphQLNonNull(GraphQLString) },
        adgenda: { type: new GraphQLList(adgendaInput) },
        leaders: { type: new GraphQLList(leaderInput) },
        ambassadors: { type: new GraphQLList(GraphQLString) },
        mission: { type: GraphQLString }
      },
      async resolve(parent, args) {
        let meetup = await Meetup.findById(args.id);
        meetup.name = args.name;
        meetup.date = args.date;
        meetup.venue = args.venue;
        meetup.food = args.food;
        meetup.drinks = args.drinks;
        meetup.adgenda = args.adgenda;
        meetup.leaders = args.leaders;
        meetup.ambassadors = args.ambassadors;
        meetup.mission = args.mission;
        meetup.save();
      }
    }
  }
});

const Query = new GraphQLObjectType({
  name: `Query`,
  fields: {
    meetups: {
      type: new GraphQLList(MeetupType),
      resolve(parent, args) {
        return Meetup.find({});
      }
    },
    meetup: {
      type: MeetupType,
      args: { _id: { type: GraphQLID }},
      resolve(parent, args) {
        if(args._id) {
          return Meetup.findById(args._id);
        } else {
          return Meetup.findOne();
        }
      }
    },
    guestsByMeetupId: {
      type: new GraphQLList(GuestType),
      args: { _id: { type: GraphQLID } },
      resolve(parent, args) {
        return Guest.find({ meetups: args._id })
      }
    }
  }
});



module.exports = new GraphQLSchema({
  mutation: Mutation,
  query: Query
})
