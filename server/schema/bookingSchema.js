const graphql = require(`graphql`);

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLInputObjectType,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull
 } = graphql;

const Meetup = require('../models/meetup');
const Guest = require('../models/guest');

const guestType = new GraphQLObjectType({
  name: `Guest`,
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: new GraphQLNonNull(GraphQLString) },
    password: { type: new GraphQLNonNull(GraphQLString) },
    email: { type: new GraphQLNonNull(GraphQLString) },
    meetups: { type: new GraphQLList(GraphQLID)},
    photo: { type: GraphQLString }
  })
})

const Mutation = new GraphQLObjectType({
  name: `Mutation`,
  fields: {
    createGuest: {
      type: guestType,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        password: { type: new GraphQLNonNull(GraphQLString) },
        email: { type: new GraphQLNonNull(GraphQLString) },
        meetups: { type: new GraphQLList(GraphQLID)},
        photo: { type: GraphQLString }
      },
      resolve(parent, args) {
        const guest = new Guest({
          name: args.name,
          password: args.password,
          email: args.email,
          photo: args.photo,
          meetups: []
        });
        return guest.save();
      }
    }
  }
});


module.exports = new GraphQLSchema({
  mutation: Mutation,
})
