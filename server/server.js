const express = require(`express`);
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const config = require('./config.json');
const cors = require('cors');
const bodyParser = require('body-parser');
const graphqlHTTP = require('express-graphql');
const schema = require(`./schema/schema`);
const userSchema = require('./schema/bookingSchema');
const mongoose = require(`mongoose`);
const Guest = require('./models/guest');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

const PORT = 4000;

mongoose.connect(config.connectionString, { useUnifiedTopology: true, useNewUrlParser: true });

app.post('/login', async (req, res) => {
  const { email, password } = req.body
  console.log(email);
  const theUser = await Guest.findOne({ email: email });

  if (!theUser) {
    res.status(404).send({
      success: false,
      message: `Could not find account: ${email}`,
    })
    return
  }
  const match = password === theUser.password;

  if (!match) {
    res.status(401).send({
      success: false,
      message: 'Incorrect credentials',
    })
    return
  }

  const token = jwt.sign(
    { email: theUser.email, id: theUser.id },
    'secret',
  )

  res.send({
    success: true,
    token: token,
  })
});

app.post('/book', async (req, res) => {

  const token = req.headers.authorization || '';
  
  console.log(req.body);
  const meetupId = req.body.meetupId;
  console.log(meetupId);

  try {
    const { id, email } = jwt.verify(token, 'secret');
    let guest = await Guest.findOne({ _id: id });

    guest.meetups = [ ...guest.meetups, meetupId ];
    guest.save();
    res.send({id, email});
  } catch (e) {
      res.send('Authentication token is invalid, please log in')
  }


});

app.use(`/`, graphqlHTTP({
  schema: schema,
  graphiql: true
}));




const dbConnection = mongoose.connection;
dbConnection.on('error', err => console.log(`Connection error: ${err}`))
dbConnection.once('open', () => console.log(`Connected to DB!`))

app.listen(PORT, err => {
  console.log(err ? error : `Server started!`)
})
