const expressJwt = require('express-jwt');
const config = require('../config.json');
const Guest = require('../models/guest');

module.exports = jwt;

function jwt() {
  const secret = config.secret;
  return expressJwt({ secret, isRevoked }).unless({
    path: [
      // public routes that don't require authentication
      '/',
    ]
  });
}

async function isRevoked(req, payload, done) {
  const user = await Guest.getById(payload.sub);

  // revoke token if user no longer exists
  if (!user) {
    return done(null, true);
  }

  done();
};
