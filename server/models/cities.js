const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const guestSchema = new Schema({
    name: String,
    firstName: Date,
    lastName: String,
    greeting: String,
    meetups: [{meetupId: String}],
    socials: [{linkedIn: String, facebook: String, instagram: String}],
    interests: [String],
    photo: String
});

module.exports = mongoose.model(`Guest`, guestSchema);