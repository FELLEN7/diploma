const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const meetupSchema = new Schema({
    name: String,
    date: String,
    venue: String,
    leaders: [
        {   name: String,
            topic: String,
            photo: String
        }
    ],
    ambassadors: [String],
    adgenda: [
        {
            time: String,
            event: String
        }
    ],
    mission: String,
    food: String,
    drinks: String,
});

module.exports = mongoose.model(`Meetup`, meetupSchema);
