const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const guestSchema = new Schema({
    name: String,
    password: String,
    email: String,
    greeting: String,
    meetups: [String],
    socials: [{linkedIn: String, facebook: String, instagram: String}],
    interests: [String],
    photo: String
});

module.exports = mongoose.model(`Guest`, guestSchema);
