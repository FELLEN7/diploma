const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const leaderSchema = new Schema({
    name: String,
    firstName: Date,
    lastName: String,
    type: String,
    socials: [{linkedIn: String, facebook: String, instagram: String}],
    meetups: [String],
    topics: [{meetupId: String, topic: String}],
    photo: String
});

module.exports = mongoose.model(`Leader`, leaderSchema);