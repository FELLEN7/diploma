const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ambassadorSchema = new Schema({
    name: String,
    firstName: String,
    lastName: String,
    meetups: [{meetupId: String}],
    photo: String
});

module.exports = mongoose.model(`Ambassador`, ambassadorSchema);
